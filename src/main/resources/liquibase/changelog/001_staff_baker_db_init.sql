CREATE TABLE employee
(
  id              SERIAL       NOT NULL
    CONSTRAINT employee_pkey
    PRIMARY KEY,
  email           VARCHAR(255) NOT NULL,
  password        VARCHAR(255) NOT NULL,
  roles           VARCHAR(255),
  phone           VARCHAR(255),
  skype           VARCHAR(255),
  telegram        VARCHAR(255),
  working_place   VARCHAR(255),
  title           VARCHAR(255),
  picture_url     VARCHAR(255),
  summary         TEXT,
  first_name      VARCHAR(255),
  last_name       VARCHAR(255),
  department      VARCHAR(255),
  gender          VARCHAR(255),
  birthday        DATE,
  active          BOOLEAN DEFAULT TRUE,
  created_at      TIMESTAMP,
  updated_at      TIMESTAMP,
  deactivated_at  TIMESTAMP,
  created_by      VARCHAR(255),
  last_updated_by VARCHAR(255)
);

CREATE UNIQUE INDEX employee_email_uindex
  ON employee (email);

CREATE INDEX employee_first_name_index
  ON employee (first_name);

CREATE INDEX employee_last_name_index
  ON employee (last_name);

CREATE INDEX employee_department_index
  ON employee (department);

CREATE TABLE technology_template
(
  id              SERIAL       NOT NULL
    CONSTRAINT technology_template_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255),
  type            VARCHAR(255)
);

CREATE UNIQUE INDEX technology_template_title_uindex
  ON technology_template (title);

CREATE TABLE project_template
(
  id              SERIAL       NOT NULL
    CONSTRAINT project_template_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  description     TEXT,
  summary         TEXT,
  created_at      TIMESTAMP,
  updated_at      TIMESTAMP,
  deactivated_at  TIMESTAMP,
  created_by      VARCHAR(255),
  last_updated_by VARCHAR(255),
  active          BOOLEAN
);

CREATE UNIQUE INDEX project_template_title_uindex
  ON project_template (title);

CREATE TABLE technology
(
  id              SERIAL NOT NULL
    CONSTRAINT technology_pkey
    PRIMARY KEY,
  title           VARCHAR(255),
  type            VARCHAR(255),
  experience      INTEGER,
  last_time_used  BIGINT,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255),
  employee_id     INTEGER
);

CREATE INDEX ix_technology_1
  ON technology (employee_id);

CREATE TABLE project
(
  id               SERIAL NOT NULL
    CONSTRAINT project_pkey
    PRIMARY KEY,
  title            VARCHAR(255),
  description      TEXT,
  summary          TEXT,
  start_work_at    TIMESTAMP,
  end_work_at      TIMESTAMP,
  position         VARCHAR(255),
  employee_id      INTEGER,
  company          VARCHAR(255),
  team_size        INTEGER,
  responsibilities TEXT,
  created_at       TIMESTAMP,
  created_by       VARCHAR(255),
  updated_at       TIMESTAMP,
  last_updated_by  VARCHAR(255)
);

CREATE UNIQUE INDEX project_id_uindex
  ON project (id);

CREATE TABLE language_template
(
  id              SERIAL       NOT NULL
    CONSTRAINT language_template_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255) NOT NULL,
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255)
);

CREATE UNIQUE INDEX language_template_title_uindex
  ON language_template (title);

CREATE TABLE language
(
  id              SERIAL       NOT NULL
    CONSTRAINT language_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  language_level  VARCHAR(255) NOT NULL,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255),
  employee_id     INTEGER
);

CREATE INDEX language_employee_id_index
  ON language (employee_id);

CREATE TABLE education
(
  id              SERIAL       NOT NULL
    CONSTRAINT education_pkey
    PRIMARY KEY,
  school          VARCHAR(255) NOT NULL,
  started_at      DATE,
  ended_at        DATE,
  degree          VARCHAR(255),
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255),
  employee_id     INTEGER
);

CREATE UNIQUE INDEX "education_school _uindex"
  ON education (school);

CREATE INDEX education_employee_id_index
  ON education (employee_id);

CREATE TABLE school_template
(
  id              SERIAL       NOT NULL
    CONSTRAINT school_template_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255)
);

CREATE UNIQUE INDEX school_template_title_uindex
  ON school_template (title);

CREATE TABLE degree_template
(
  id              SERIAL       NOT NULL
    CONSTRAINT degree_template_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  created_at      TIMESTAMP,
  created_by      VARCHAR(255),
  updated_at      TIMESTAMP,
  last_updated_by VARCHAR(255)
);

CREATE UNIQUE INDEX degree_template_title_uindex
  ON degree_template (title);

CREATE TABLE project_technology
(
  id              SERIAL       NOT NULL
    CONSTRAINT project_technology_pkey
    PRIMARY KEY,
  title           VARCHAR(255) NOT NULL,
  type            VARCHAR(255) NOT NULL,
  project_id      BIGINT,
  created_at      TIMESTAMP,
  updated_at      TIMESTAMP,
  created_by      VARCHAR(255),
  last_updated_by VARCHAR(255)
);

CREATE UNIQUE INDEX project_technology_id_uindex
  ON project_technology (id);

CREATE INDEX project_technology_title_index
  ON project_technology (title);

CREATE TABLE spring_session
(
  session_id            CHAR(36) NOT NULL
    CONSTRAINT spring_session_pk
    PRIMARY KEY,
  creation_time         BIGINT   NOT NULL,
  last_access_time      BIGINT   NOT NULL,
  max_inactive_interval INTEGER  NOT NULL,
  principal_name        VARCHAR(100)
);

CREATE INDEX spring_session_ix1
  ON spring_session (last_access_time);

CREATE TABLE spring_session_attributes
(
  session_id      CHAR(36)     NOT NULL
    CONSTRAINT spring_session_attributes_fk
    REFERENCES spring_session
    ON DELETE CASCADE,
  attribute_name  VARCHAR(200) NOT NULL,
  attribute_bytes BYTEA        NOT NULL,
  CONSTRAINT spring_session_attributes_pk
  PRIMARY KEY (session_id, attribute_name)
);

CREATE INDEX spring_session_attributes_ix1
  ON spring_session_attributes (session_id);
