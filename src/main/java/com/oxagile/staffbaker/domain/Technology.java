package com.oxagile.staffbaker.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "technology")
public class Technology extends Auditable implements Serializable {
    private static final long serialVersionUID = -1276450417372537088L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Enumerated(EnumType.STRING)
    private TechnologyType type;

    @Column(name = "experience")
    private Integer experience;

    @Column(name = "last_time_used")
    private Long lastTimeUsed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Technology() {
    }

    private Technology(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setType(builder.type);
        setExperience(builder.experience);
        setLastTimeUsed(builder.lastTimeUsed);
        setEmployee(builder.employee);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TechnologyType getType() {
        return type;
    }

    public void setType(TechnologyType type) {
        this.type = type;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Long getLastTimeUsed() {
        return lastTimeUsed;
    }

    public void setLastTimeUsed(Long lastTimeUsed) {
        this.lastTimeUsed = lastTimeUsed;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Technology that = (Technology) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(type, that.type) &&
                Objects.equals(experience, that.experience) &&
                Objects.equals(lastTimeUsed, that.lastTimeUsed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, type, experience, lastTimeUsed);
    }

    @Override
    public String toString() {
        return "Technology{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", experience=" + experience +
                ", lastTimeUsed=" + lastTimeUsed +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String title;
        private TechnologyType type;
        private Integer experience;
        private Long lastTimeUsed;
        private Employee employee;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(TechnologyType val) {
            type = val;
            return this;
        }

        public Builder withExperience(Integer val) {
            experience = val;
            return this;
        }

        public Builder withLastTimeUsed(Long val) {
            lastTimeUsed = val;
            return this;
        }

        public Builder withEmployee(Employee val) {
            employee = val;
            return this;
        }

        public Technology build() {
            return new Technology(this);
        }
    }
}