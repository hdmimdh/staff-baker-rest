package com.oxagile.staffbaker.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "language")
public class Language extends Auditable implements Serializable {

    private static final long serialVersionUID = 7771324634434518580L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true)
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "language_level")
    private LanguageLevel languageLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Language() {
    }

    private Language(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setLanguageLevel(builder.languageLevel);
        setEmployee(builder.employee);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LanguageLevel getLanguageLevel() {
        return languageLevel;
    }

    public void setLanguageLevel(LanguageLevel languageLevel) {
        this.languageLevel = languageLevel;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Language that = (Language) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(languageLevel, that.languageLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, languageLevel);
    }

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", languageLevel=" + languageLevel +
                ", employee=" + employee +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String title;
        private LanguageLevel languageLevel;
        private Employee employee;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withLanguageLevel(LanguageLevel val) {
            languageLevel = val;
            return this;
        }

        public Builder withEmployee(Employee val) {
            employee = val;
            return this;
        }

        public Language build() {
            return new Language(this);
        }
    }
}
