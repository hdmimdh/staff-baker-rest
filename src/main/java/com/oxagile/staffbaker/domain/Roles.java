package com.oxagile.staffbaker.domain;

public enum Roles {
    
    ADMIN, USER;

    @Override
    public String toString() {
        return this.name().toUpperCase();
    }
}
