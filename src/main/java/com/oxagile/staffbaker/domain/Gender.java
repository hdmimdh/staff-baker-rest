package com.oxagile.staffbaker.domain;

public enum Gender {

    MALE, FEMALE
}
