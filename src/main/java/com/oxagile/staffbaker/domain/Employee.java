package com.oxagile.staffbaker.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "employee")
public class Employee extends Auditable implements Serializable, UserDetails {
    private static final long serialVersionUID = -3723711319019568097L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "roles")
    private String roles;

    @JsonIgnore
    @Column(name = "first_name")
    private String firstName;

    @JsonIgnore
    @Column(name = "last_name")
    private String lastName;

    @JsonIgnore
    @Column(name = "department")
    @Enumerated(EnumType.STRING)
    private Department department;

    @JsonIgnore
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @JsonIgnore
    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @Column(name = "skype")
    private String skype;

    @JsonIgnore
    @Column(name = "telegram")
    private String telegram;

    @JsonIgnore
    @Column(name = "workingPlace")
    private String workingPlace;

    @JsonIgnore
    @Column(name = "title")
    private String title;

    @JsonIgnore
    @Column(name = "pictureUrl")
    private String pictureUrl;

    @JsonIgnore
    @Column(name = "summary")
    private String summary;

    @JsonIgnore
    @Column(name = "birthday")
    private LocalDate birthday;

    @JsonIgnore
    @Column(name = "active")
    private Boolean active;

    @JsonIgnore
    @Column(name = "deactivated_at")
    private ZonedDateTime deactivatedAt;

    @JsonIgnore
    @OneToMany(
            mappedBy = "employee",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Technology> technologies;

    @JsonIgnore
    @OneToMany(
            mappedBy = "employee",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Language> languages;

    @JsonIgnore
    @OneToMany(
            mappedBy = "employee",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Education> educations;


    @JsonIgnore
    @OneToMany(
            mappedBy = "employee",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Project> projects;


    public Employee() {
    }

    private Employee(Builder builder) {
        setId(builder.id);
        setEmail(builder.email);
        setPassword(builder.password);
        setRoles(builder.roles);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setDepartment(builder.department);
        setGender(builder.gender);
        setPhone(builder.phone);
        setSkype(builder.skype);
        setTelegram(builder.telegram);
        setWorkingPlace(builder.workingPlace);
        setTitle(builder.title);
        setPictureUrl(builder.pictureUrl);
        setSummary(builder.summary);
        setBirthday(builder.birthday);
        setActive(builder.active);
        setDeactivatedAt(builder.deactivatedAt);
        setTechnologies(builder.technologies);
        setLanguages(builder.languages);
        setEducations(builder.educations);
        setProjects(builder.projects);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getWorkingPlace() {
        return workingPlace;
    }

    public void setWorkingPlace(String workingPlace) {
        this.workingPlace = workingPlace;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ZonedDateTime getDeactivatedAt() {
        return deactivatedAt;
    }

    public void setDeactivatedAt(ZonedDateTime deactivatedAt) {
        this.deactivatedAt = deactivatedAt;
    }

    public Set<Technology> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(Set<Technology> technologies) {
        this.technologies = technologies;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public void setEducations(Set<Education> educations) {
        this.educations = educations;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee that = (Employee) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(roles, that.roles) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(department, that.department) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(skype, that.skype) &&
                Objects.equals(telegram, that.telegram) &&
                Objects.equals(workingPlace, that.workingPlace) &&
                Objects.equals(title, that.title) &&
                Objects.equals(pictureUrl, that.pictureUrl) &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, roles, firstName, lastName, department, gender, phone, skype, telegram, workingPlace, title, pictureUrl, summary, birthday, active);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", roles='" + roles + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", department=" + department +
                ", gender=" + gender +
                ", phone='" + phone + '\'' +
                ", skype='" + skype + '\'' +
                ", telegram='" + telegram + '\'' +
                ", workingPlace='" + workingPlace + '\'' +
                ", title='" + title + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", summary='" + summary + '\'' +
                ", birthday=" + birthday +
                ", active=" + active +
                ", deactivatedAt=" + deactivatedAt +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String email;
        private String password;
        private String roles;
        private String firstName;
        private String lastName;
        private Department department;
        private Gender gender;
        private String phone;
        private String skype;
        private String telegram;
        private String workingPlace;
        private String title;
        private String pictureUrl;
        private String summary;
        private LocalDate birthday;
        private Boolean active;
        private ZonedDateTime deactivatedAt;
        private Set<Technology> technologies;
        private Set<Language> languages;
        private Set<Education> educations;
        private Set<Project> projects;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withPassword(String val) {
            password = val;
            return this;
        }

        public Builder withRoles(String val) {
            roles = val;
            return this;
        }

        public Builder withFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder withLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder withDepartment(Department val) {
            department = val;
            return this;
        }

        public Builder withGender(Gender val) {
            gender = val;
            return this;
        }

        public Builder withPhone(String val) {
            phone = val;
            return this;
        }

        public Builder withSkype(String val) {
            skype = val;
            return this;
        }

        public Builder withTelegram(String val) {
            telegram = val;
            return this;
        }

        public Builder withWorkingPlace(String val) {
            workingPlace = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withPictureUrl(String val) {
            pictureUrl = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withBirthday(LocalDate val) {
            birthday = val;
            return this;
        }

        public Builder withActive(Boolean val) {
            active = val;
            return this;
        }

        public Builder withDeactivatedAt(ZonedDateTime val) {
            deactivatedAt = val;
            return this;
        }

        public Builder withTechnologies(Set<Technology> val) {
            technologies = val;
            return this;
        }

        public Builder withLanguages(Set<Language> val) {
            languages = val;
            return this;
        }

        public Builder withEducation(Set<Education> val) {
            educations = val;
            return this;
        }

        public Builder withProjects(Set<Project> val) {
            projects = val;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        if (StringUtils.isBlank(this.roles)) {
            return new ArrayList<>();
        }

        return Arrays.stream(roles.split(","))
                .map(role -> new SimpleGrantedAuthority(role.toUpperCase()))
                .collect(Collectors.toList());
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return this.email;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
