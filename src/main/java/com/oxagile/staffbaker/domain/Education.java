package com.oxagile.staffbaker.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "education")
public class Education extends Auditable implements Serializable {

    private static final long serialVersionUID = 3035055043886965427L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "school", nullable = false, unique = true)
    private String school;

    @Column(name = "started_at")
    private LocalDate startedAt;

    @Column(name = "ended_at")
    private LocalDate endedAt;

    @Column(name = "degree")
    private String degree;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Education() {
    }

    private Education(Builder builder) {
        setId(builder.id);
        setSchool(builder.school);
        setStartedAt(builder.startedAt);
        setEndedAt(builder.endedAt);
        setDegree(builder.degree);
        setEmployee(builder.employee);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDate getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDate startedAt) {
        this.startedAt = startedAt;
    }

    public LocalDate getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(LocalDate endedAt) {
        this.endedAt = endedAt;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Education that = (Education) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(school, that.school) &&
                Objects.equals(startedAt, that.startedAt) &&
                Objects.equals(endedAt, that.endedAt) &&
                Objects.equals(degree, that.degree);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, school, startedAt, endedAt, degree);
    }

    @Override
    public String toString() {
        return "Education{" +
                "id=" + id +
                ", school='" + school + '\'' +
                ", startedAt=" + startedAt +
                ", endedAt=" + endedAt +
                ", degree='" + degree + '\'' +
                '}';
    }

    public static final class Builder {
        private Long id;
        private String school;
        private LocalDate startedAt;
        private LocalDate endedAt;
        private String degree;
        private Employee employee;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withSchool(String val) {
            school = val;
            return this;
        }

        public Builder withStartedAt(LocalDate val) {
            startedAt = val;
            return this;
        }

        public Builder withEndedAt(LocalDate val) {
            endedAt = val;
            return this;
        }

        public Builder withDegree(String val) {
            degree = val;
            return this;
        }

        public Builder withEmployee(Employee val) {
            employee = val;
            return this;
        }

        public Education build() {
            return new Education(this);
        }
    }
}
