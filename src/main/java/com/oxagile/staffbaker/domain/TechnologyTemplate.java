package com.oxagile.staffbaker.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "technology_template")
public class TechnologyTemplate extends Auditable implements Serializable {
    private static final long serialVersionUID = 3075156424370863279L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true)
    private String title;

    @Enumerated(EnumType.STRING)
    private TechnologyType type;

    public TechnologyTemplate() {
    }

    private TechnologyTemplate(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        type = builder.type;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TechnologyType getType() {
        return type;
    }

    public void setType(TechnologyType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechnologyTemplate that = (TechnologyTemplate) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, type);
    }

    @Override
    public String toString() {
        return "TechnologyTemplate{" +
                "id=" + id +
                ", title=" + title +
                ", type=" + type +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String title;
        private TechnologyType type;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(TechnologyType val) {
            type = val;
            return this;
        }

        public TechnologyTemplate build() {
            return new TechnologyTemplate(this);
        }
    }
}
