package com.oxagile.staffbaker.domain;

public enum Department {

    JAVA, NET, RUBY, JS
}
