package com.oxagile.staffbaker.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "project_technology")
public class ProjectTechnology extends Auditable implements Serializable {

    private static final long serialVersionUID = 1421452325638124549L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TechnologyType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    public ProjectTechnology() {
    }

    private ProjectTechnology(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setType(builder.type);
        setProject(builder.project);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TechnologyType getType() {
        return type;
    }

    public void setType(TechnologyType type) {
        this.type = type;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectTechnology that = (ProjectTechnology) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                type == that.type &&
                Objects.equals(project, that.project);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, type, project);
    }


    public static final class Builder {
        private Long id;
        private String title;
        private TechnologyType type;
        private Project project;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(TechnologyType val) {
            type = val;
            return this;
        }

        public Builder withProject(Project val) {
            project = val;
            return this;
        }

        public ProjectTechnology build() {
            return new ProjectTechnology(this);
        }
    }
}
