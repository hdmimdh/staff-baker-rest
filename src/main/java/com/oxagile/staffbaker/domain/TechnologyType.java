package com.oxagile.staffbaker.domain;

import com.oxagile.staffbaker.dto.PairDto;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum TechnologyType {

    PROGRAMMING_TECH("Programming Technologies"),
    PROGRAMMING_FRAMEWORK("Programming Frameworks"),
    DEV_METHODOLOGY("Development Methodologies and Practices"),
    DB_TECH("Database Management System"),
    APP_SERVER("Application Servers"),
    OPERATION_SYS("Operating Systems"),
    CI("Continuous Integration"),
    SERVICE_TOOL("Services and Tools ");

    private String description;

    TechnologyType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static List<String> getTechnologyTypes() {
        return Arrays.stream(TechnologyType.values())
                .map(type -> type.description)
                .collect(Collectors.toList());

    }

    public static List<PairDto> getTechnologies() {
        return Arrays.stream(TechnologyType.values())
                .map(type -> new PairDto<>(type.name(), type.getDescription()))
                .collect(Collectors.toList());
    }
}
