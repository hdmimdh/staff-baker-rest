package com.oxagile.staffbaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "project_template")
public class ProjectTemplate extends Auditable implements Serializable {

    private static final long serialVersionUID = 5643181235455267913L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @Column(name = "title", nullable = false, unique = true)
    private String title;

    @JsonIgnore
    @Column(name = "description")
    private String description;

    @JsonIgnore
    @Column(name = "summary")
    private String summary;

    @JsonIgnore
    @Column(name = "active")
    private Boolean active;

    @JsonIgnore
    @Column(name = "deactivated_at")
    private ZonedDateTime deactivatedAt;

    public ProjectTemplate() {
    }

    private ProjectTemplate(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
        setActive(builder.active);
        setDeactivatedAt(builder.deactivatedAt);
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ZonedDateTime getDeactivatedAt() {
        return deactivatedAt;
    }

    public void setDeactivatedAt(ZonedDateTime deactivatedAt) {
        this.deactivatedAt = deactivatedAt;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectTemplate that = (ProjectTemplate) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(active, that.active) &&
                Objects.equals(deactivatedAt, that.deactivatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, summary, active, deactivatedAt);
    }


    public static final class Builder {
        private Long id;
        private String title;
        private String description;
        private String summary;
        private Boolean active;
        private ZonedDateTime deactivatedAt;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withActive(Boolean val) {
            active = val;
            return this;
        }

        public Builder withDeactivatedAt(ZonedDateTime val) {
            deactivatedAt = val;
            return this;
        }

        public ProjectTemplate build() {
            return new ProjectTemplate(this);
        }
    }
}
