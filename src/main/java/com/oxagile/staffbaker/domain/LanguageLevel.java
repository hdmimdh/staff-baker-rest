package com.oxagile.staffbaker.domain;

import com.oxagile.staffbaker.dto.PairDto;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum LanguageLevel {

    A1("Beginner"),
    A2("Elementary"),
    B1("Intermediate"),
    B2("Upper-Intermediate"),
    C1("Advanced"),
    C2("Proficiency"),
    NATIVE("Native");

    private String description;

    LanguageLevel(String description) {
        this.description = description;
    }

    public static List<String> getLanguageLevels() {
        return Arrays.stream(LanguageLevel.values())
                .map(type -> type.description)
                .collect(Collectors.toList());

    }

    public String getDescription() {
        return description;
    }

    public static List<PairDto> getLanguages() {
        return Arrays.stream(LanguageLevel.values())
                .map(level -> new PairDto<>(level.name(), level.getDescription()))
                .collect(Collectors.toList());
    }
}
