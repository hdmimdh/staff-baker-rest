package com.oxagile.staffbaker.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "language_template")
public class LanguageTemplate extends Auditable implements Serializable {

    private static final long serialVersionUID = 7339853428381884620L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true)
    private String title;

    public LanguageTemplate() {
    }

    private LanguageTemplate(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LanguageTemplate that = (LanguageTemplate) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

    @Override
    public String toString() {
        return "LanguageTemplate{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String title;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public LanguageTemplate build() {
            return new LanguageTemplate(this);
        }
    }
}
