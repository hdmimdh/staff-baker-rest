package com.oxagile.staffbaker.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "project")
public class Project extends Auditable implements Serializable {

    private static final long serialVersionUID = 8712930794810798491L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "summary")
    private String summary;

    @Column(name = "company")
    private String company;

    @Column(name = "team_size")
    private Integer teamSize;

    @Column(name = "start_work_at")
    private ZonedDateTime startWorkAt;

    @Column(name = "end_work_at")
    private ZonedDateTime endWorkAt;

    @Column(name = "position")
    private String position;

    @Column(name = "responsibilities")
    private String responsibilities;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<ProjectTechnology> projectTechnologies;

    public Project() {
    }

    private Project(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
        setCompany(builder.company);
        setTeamSize(builder.teamSize);
        setStartWorkAt(builder.startWorkAt);
        setEndWorkAt(builder.endWorkAt);
        setPosition(builder.position);
        setResponsibilities(builder.responsibilities);
        setEmployee(builder.employee);
        setProjectTechnologies(builder.projectTechnologies);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(Integer teamSize) {
        this.teamSize = teamSize;
    }

    public ZonedDateTime getStartWorkAt() {
        return startWorkAt;
    }

    public void setStartWorkAt(ZonedDateTime startWorkAt) {
        this.startWorkAt = startWorkAt;
    }

    public ZonedDateTime getEndWorkAt() {
        return endWorkAt;
    }

    public void setEndWorkAt(ZonedDateTime endWorkAt) {
        this.endWorkAt = endWorkAt;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<ProjectTechnology> getProjectTechnologies() {
        return projectTechnologies;
    }

    public void setProjectTechnologies(List<ProjectTechnology> projectTechnologies) {
        this.projectTechnologies = projectTechnologies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(id, project.id) &&
                Objects.equals(title, project.title) &&
                Objects.equals(description, project.description) &&
                Objects.equals(summary, project.summary) &&
                Objects.equals(company, project.company) &&
                Objects.equals(teamSize, project.teamSize) &&
                Objects.equals(startWorkAt, project.startWorkAt) &&
                Objects.equals(endWorkAt, project.endWorkAt) &&
                Objects.equals(position, project.position) &&
                Objects.equals(responsibilities, project.responsibilities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, summary, company, teamSize, startWorkAt, endWorkAt, position, responsibilities);
    }

    public static final class Builder {
        private Long id;
        private String title;
        private String description;
        private String summary;
        private String company;
        private Integer teamSize;
        private ZonedDateTime startWorkAt;
        private ZonedDateTime endWorkAt;
        private String position;
        private String responsibilities;
        private Employee employee;
        private List<ProjectTechnology> projectTechnologies;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withCompany(String val) {
            company = val;
            return this;
        }

        public Builder withTeamSize(Integer val) {
            teamSize = val;
            return this;
        }

        public Builder withStartWorkAt(ZonedDateTime val) {
            startWorkAt = val;
            return this;
        }

        public Builder withEndWorkAt(ZonedDateTime val) {
            endWorkAt = val;
            return this;
        }

        public Builder withPosition(String val) {
            position = val;
            return this;
        }

        public Builder withResponsibilities(String val) {
            responsibilities = val;
            return this;
        }

        public Builder withEmployee(Employee val) {
            employee = val;
            return this;
        }

        public Builder withProjectTechnologies(List<ProjectTechnology> val) {
            projectTechnologies = val;
            return this;
        }

        public Project build() {
            return new Project(this);
        }
    }
}
