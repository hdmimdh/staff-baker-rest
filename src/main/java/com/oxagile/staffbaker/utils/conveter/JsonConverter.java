package com.oxagile.staffbaker.utils.conveter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class JsonConverter {
    private static final Logger LOGGER = LogManager.getLogger(JsonConverter.class);

    private final ObjectMapper objectMapper;

    @Autowired
    public JsonConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String convert(Object val) {
        try {

            return objectMapper.writeValueAsString(val);

        } catch (Exception e) {

            LOGGER.error("Unable to convert Object of {} to String due to {}", val, e.getMessage());
            return null;
        }
    }

    public <T> T reconvert(String val, Class<T> clazz) {
        try {

            return objectMapper.readValue(val, clazz);

        } catch (Exception e) {

            LOGGER.error("Unable to reconvert String of {} to {} due to {}", val, clazz, e.getMessage());
            return null;
        }
    }
}
