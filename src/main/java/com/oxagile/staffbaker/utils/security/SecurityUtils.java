package com.oxagile.staffbaker.utils.security;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.oxagile.staffbaker.domain.Employee;

@Component
public class SecurityUtils {

    private static final Logger LOGGER = LogManager.getLogger(SecurityUtils.class);


    public void validateCurrentUser(Long id, String action) {

        Employee currentEmployee = (Employee) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!Objects.equals(currentEmployee.getId(), id)) {

            LOGGER.error("Unable to " + action + " with id of - {} on behalf of a different user with id of - {}", id, currentEmployee.getId());
            throw new AccessDeniedException("Unable to " + action + " on behalf of a different user");
        }
    }

}
