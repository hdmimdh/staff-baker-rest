package com.oxagile.staffbaker.utils.common;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class EnumUtils {

    public <T extends Enum<T>> List<String> asList(Class<T> enumType) {
        return Arrays.stream(enumType.getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
    }
}
