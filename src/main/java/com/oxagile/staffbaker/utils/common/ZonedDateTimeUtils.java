package com.oxagile.staffbaker.utils.common;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Component
public class ZonedDateTimeUtils {

    private static final DateTimeFormatter ISO_INSTANT_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX").withZone(ZoneOffset.UTC);

    public ZonedDateTime toZonedDateTime(String zuluStr) {
        return ZonedDateTime.parse(zuluStr, ISO_INSTANT_FORMAT);
    }

    public Long toEpochMilli(String zuluStr) {
        return ZonedDateTime.parse(zuluStr, ISO_INSTANT_FORMAT).toInstant().toEpochMilli();
    }

    public String toZuluString(ZonedDateTime zonedDateTime) {
        return ISO_INSTANT_FORMAT.format(zonedDateTime);
    }

    public String toZuluString(long epochMillis) {

        Instant instant = Instant.ofEpochMilli(epochMillis);
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);

        return ISO_INSTANT_FORMAT.format(zonedDateTime);
    }

    public long duration(ZonedDateTime d1, ZonedDateTime d2, ChronoUnit unit) {
        return unit.between(d1, d2);
    }

    public String durationString(ZonedDateTime d1, ZonedDateTime d2, ChronoUnit unit) {
        return String.valueOf(unit.between(d1, d2));
    }
}
