package com.oxagile.staffbaker.utils.export;

public final class ExportPlaceholder {

    public static final String USERNAME_PLC_HOLDER = "${username}";
    public static final String TITLE_PLC_HOLDER = "${title}";
    public static final String SUMMARY_PLC_HOLDER = "${summary}";
    public static final String TECHNOLOGIES_PLC_HOLDER = "${technologies}";
    public static final String LANGUAGES_PLC_HOLDER = "${languages}";
    public static final String EDUCATIONS_PLC_HOLDER = "${educations}";
    public static final String PROJECTS_PLC_HOLDER = "${projects}";

    private ExportPlaceholder() {
        throw new AssertionError();
    }
}
