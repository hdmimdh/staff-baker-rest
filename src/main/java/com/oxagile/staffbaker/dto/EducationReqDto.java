package com.oxagile.staffbaker.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.oxagile.staffbaker.dto.converter.ZuluLocalDateDeserializer;

import java.time.LocalDate;

public class EducationReqDto {

    private String school;

    @JsonDeserialize(using = ZuluLocalDateDeserializer.class)
    private LocalDate startedAt;

    @JsonDeserialize(using = ZuluLocalDateDeserializer.class)
    private LocalDate endedAt;

    private String degree;

    private Long employeeId;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDate getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDate startedAt) {
        this.startedAt = startedAt;
    }

    public LocalDate getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(LocalDate endedAt) {
        this.endedAt = endedAt;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
