package com.oxagile.staffbaker.dto;

public class SchoolTemplateReqDto {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SchoolTemplateReqDto{" +
                "title=" + title +
                '}';
    }
}
