package com.oxagile.staffbaker.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oxagile.staffbaker.dto.converter.ZuluDateTimeSerializer;

import java.time.ZonedDateTime;
import java.util.List;

public class ProjectResponseDto {

    private Long id;

    private String title;

    private String description;

    private String summary;

    private String company;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime startWorkAt;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime endWorkAt;

    private String position;

    private Integer teamSize;

    private String responsibilities;

    private List<ProjectTechnologyDto> projectTechnologies;

    private ProjectResponseDto(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
        setCompany(builder.company);
        setStartWorkAt(builder.startWorkAt);
        setEndWorkAt(builder.endWorkAt);
        setPosition(builder.position);
        setTeamSize(builder.teamSize);
        setResponsibilities(builder.responsibilities);
        setProjectTechnologies(builder.projectTechnologies);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public ZonedDateTime getStartWorkAt() {
        return startWorkAt;
    }

    public void setStartWorkAt(ZonedDateTime startWorkAt) {
        this.startWorkAt = startWorkAt;
    }

    public ZonedDateTime getEndWorkAt() {
        return endWorkAt;
    }

    public void setEndWorkAt(ZonedDateTime endWorkAt) {
        this.endWorkAt = endWorkAt;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(Integer teamSize) {
        this.teamSize = teamSize;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public List<ProjectTechnologyDto> getProjectTechnologies() {
        return projectTechnologies;
    }

    public void setProjectTechnologies(List<ProjectTechnologyDto> projectTechnologies) {
        this.projectTechnologies = projectTechnologies;
    }

    public static final class Builder {
        private Long id;
        private String title;
        private String description;
        private String summary;
        private String company;
        private ZonedDateTime startWorkAt;
        private ZonedDateTime endWorkAt;
        private String position;
        private Integer teamSize;
        private String responsibilities;
        private List<ProjectTechnologyDto> projectTechnologies;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withCompany(String val) {
            company = val;
            return this;
        }

        public Builder withStartWorkAt(ZonedDateTime val) {
            startWorkAt = val;
            return this;
        }

        public Builder withEndWorkAt(ZonedDateTime val) {
            endWorkAt = val;
            return this;
        }

        public Builder withPosition(String val) {
            position = val;
            return this;
        }

        public Builder withTeamSize(Integer val) {
            teamSize = val;
            return this;
        }

        public Builder withResponsibilities(String val) {
            responsibilities = val;
            return this;
        }

        public Builder withProjectTechnologies(List<ProjectTechnologyDto> val) {
            projectTechnologies = val;
            return this;
        }

        public ProjectResponseDto build() {
            return new ProjectResponseDto(this);
        }
    }
}
