package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.NotBlank;

public class CredentialsDto {

    @NotBlank(message = "{email.required.violation}")
    private String email;

    @NotBlank(message = "{password.required.violation}")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "CredentialsDto{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
