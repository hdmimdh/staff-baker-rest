package com.oxagile.staffbaker.dto;

import com.oxagile.staffbaker.domain.TechnologyType;

public class ProjectTechnologyDto {

    private String title;
    private TechnologyType type;

    public ProjectTechnologyDto() {
    }

    public ProjectTechnologyDto(Long id, String title, TechnologyType type, Long projectId) {
        this.title = title;
        this.type = type;
    }

    private ProjectTechnologyDto(Builder builder) {
        setTitle(builder.title);
        setType(builder.type);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TechnologyType getType() {
        return type;
    }

    public void setType(TechnologyType type) {
        this.type = type;
    }


    public static final class Builder {
        private String title;
        private TechnologyType type;

        private Builder() {
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(TechnologyType val) {
            type = val;
            return this;
        }

        public ProjectTechnologyDto build() {
            return new ProjectTechnologyDto(this);
        }
    }
}
