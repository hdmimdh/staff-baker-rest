package com.oxagile.staffbaker.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oxagile.staffbaker.domain.Department;
import com.oxagile.staffbaker.domain.Gender;
import com.oxagile.staffbaker.dto.converter.ZuluDateTimeSerializer;
import com.oxagile.staffbaker.dto.converter.ZuluLocalDateSerializer;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

public class EmployeeRespDto {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Department department;
    private Gender gender;
    private String phone;
    private String skype;
    private String telegram;
    private String workingPlace;
    private String title;
    private String pictureUrl;
    private String summary;

    @JsonSerialize(using = ZuluLocalDateSerializer.class)
    private LocalDate birthday;
    private Boolean active;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime createdAt;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime updatedAt;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime deactivatedAt;

    private String createdBy;
    private String lastUpdatedBy;

    private List<LanguageDto> languages;

    private List<EducationRespDto> educations;

    public EmployeeRespDto() {
    }

    private EmployeeRespDto(Builder builder) {
        setId(builder.id);
        setEmail(builder.email);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setDepartment(builder.department);
        setGender(builder.gender);
        setPhone(builder.phone);
        setSkype(builder.skype);
        setTelegram(builder.telegram);
        setWorkingPlace(builder.workingPlace);
        setTitle(builder.title);
        setPictureUrl(builder.pictureUrl);
        setSummary(builder.summary);
        setBirthday(builder.birthday);
        setActive(builder.active);
        setCreatedAt(builder.createdAt);
        setUpdatedAt(builder.updatedAt);
        setDeactivatedAt(builder.deactivatedAt);
        setCreatedBy(builder.createdBy);
        setLastUpdatedBy(builder.lastUpdatedBy);
        setLanguages(builder.languages);
        setEducations(builder.educations);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getWorkingPlace() {
        return workingPlace;
    }

    public void setWorkingPlace(String workingPlace) {
        this.workingPlace = workingPlace;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getDeactivatedAt() {
        return deactivatedAt;
    }

    public void setDeactivatedAt(ZonedDateTime deactivatedAt) {
        this.deactivatedAt = deactivatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public List<LanguageDto> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageDto> languages) {
        this.languages = languages;
    }

    public List<EducationRespDto> getEducations() {
        return educations;
    }

    public void setEducations(List<EducationRespDto> educations) {
        this.educations = educations;
    }

    @Override
    public String toString() {
        return "EmployeeRespDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", department=" + department +
                ", gender=" + gender +
                ", phone='" + phone + '\'' +
                ", skype='" + skype + '\'' +
                ", telegram='" + telegram + '\'' +
                ", workingPlace='" + workingPlace + '\'' +
                ", title='" + title + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", summary='" + summary + '\'' +
                ", birthday=" + birthday +
                ", active=" + active +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deactivatedAt=" + deactivatedAt +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                '}';
    }

    public static final class Builder {
        private Long id;
        private String email;
        private String token;
        private String firstName;
        private String lastName;
        private Department department;
        private Gender gender;
        private String phone;
        private String skype;
        private String telegram;
        private String workingPlace;
        private String title;
        private String pictureUrl;
        private String summary;
        private LocalDate birthday;
        private Boolean active;
        private ZonedDateTime createdAt;
        private ZonedDateTime updatedAt;
        private ZonedDateTime deactivatedAt;
        private String createdBy;
        private String lastUpdatedBy;
        private List<LanguageDto> languages;
        private List<EducationRespDto> educations;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder withLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder withDepartment(Department val) {
            department = val;
            return this;
        }

        public Builder withGender(Gender val) {
            gender = val;
            return this;
        }

        public Builder withPhone(String val) {
            phone = val;
            return this;
        }

        public Builder withSkype(String val) {
            skype = val;
            return this;
        }

        public Builder withTelegram(String val) {
            telegram = val;
            return this;
        }

        public Builder withWorkingPlace(String val) {
            workingPlace = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withPictureUrl(String val) {
            pictureUrl = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withBirthday(LocalDate val) {
            birthday = val;
            return this;
        }

        public Builder withActive(Boolean val) {
            active = val;
            return this;
        }

        public Builder withCreatedAt(ZonedDateTime val) {
            createdAt = val;
            return this;
        }

        public Builder withUpdatedAt(ZonedDateTime val) {
            updatedAt = val;
            return this;
        }

        public Builder withDeactivatedAt(ZonedDateTime val) {
            deactivatedAt = val;
            return this;
        }

        public Builder withCreatedBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder withLastUpdatedBy(String val) {
            lastUpdatedBy = val;
            return this;
        }

        public Builder withLanguages(List<LanguageDto> val) {
            languages = val;
            return this;
        }

        public Builder withEducations(List<EducationRespDto> val) {
            educations = val;
            return this;
        }

        public EmployeeRespDto build() {
            return new EmployeeRespDto(this);
        }
    }
}
