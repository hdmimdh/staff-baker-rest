package com.oxagile.staffbaker.dto;

public class LanguageDto {

    private String title;

    private String languageLevel;

    public LanguageDto() {
    }

    private LanguageDto(Builder builder) {
        setTitle(builder.title);
        setLanguageLevel(builder.languageLevel);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguageLevel() {
        return languageLevel;
    }

    public void setLanguageLevel(String languageLevel) {
        this.languageLevel = languageLevel;
    }


    public static final class Builder {
        private String title;
        private String languageLevel;

        private Builder() {
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withLanguageLevel(String val) {
            languageLevel = val;
            return this;
        }

        public LanguageDto build() {
            return new LanguageDto(this);
        }
    }
}
