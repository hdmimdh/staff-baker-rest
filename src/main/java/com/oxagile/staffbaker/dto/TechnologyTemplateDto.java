package com.oxagile.staffbaker.dto;

public class TechnologyTemplateDto {

    private Long id;
    private String title;
    private String type;
    private String description;

    public TechnologyTemplateDto() {
    }

    private TechnologyTemplateDto(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setType(builder.type);
        setDescription(builder.description);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final class Builder {
        private Long id;
        private String title;
        private String type;
        private String description;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(String val) {
            type = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public TechnologyTemplateDto build() {
            return new TechnologyTemplateDto(this);
        }
    }
}
