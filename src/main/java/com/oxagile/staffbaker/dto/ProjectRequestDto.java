package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ProjectRequestDto {

    private Long id;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @NotBlank
    private String summary;

    @NotBlank
    private String company;

    @NotNull
    private Integer teamSize;

    @NotBlank
    private String startWorkAt;

    @NotBlank
    private String endWorkAt;

    @NotBlank
    private String position;

    @NotBlank
    private String responsibilities;

    @NotNull
    private Long employeeId;

    private List<ProjectTechnologyDto> projectTechnologies;

    public ProjectRequestDto() {
    }

    private ProjectRequestDto(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
        setCompany(builder.company);
        setTeamSize(builder.teamSize);
        setStartWorkAt(builder.startWorkAt);
        setEndWorkAt(builder.endWorkAt);
        setPosition(builder.position);
        setResponsibilities(builder.responsibilities);
        setEmployeeId(builder.employeeId);
        setProjectTechnologies(builder.projectTechnologies);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStartWorkAt() {
        return startWorkAt;
    }

    public void setStartWorkAt(String startWorkAt) {
        this.startWorkAt = startWorkAt;
    }

    public String getEndWorkAt() {
        return endWorkAt;
    }

    public void setEndWorkAt(String endWorkAt) {
        this.endWorkAt = endWorkAt;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(Integer teamSize) {
        this.teamSize = teamSize;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public List<ProjectTechnologyDto> getProjectTechnologies() {
        return projectTechnologies;
    }

    public void setProjectTechnologies(List<ProjectTechnologyDto> projectTechnologies) {
        this.projectTechnologies = projectTechnologies;
    }

    public static final class Builder {
        private Long id;
        private String title;
        private String description;
        private String summary;
        private String company;
        private Integer teamSize;
        private String startWorkAt;
        private String endWorkAt;
        private String position;
        private String responsibilities;
        private Long employeeId;
        private List<ProjectTechnologyDto> projectTechnologies;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withCompany(String val) {
            company = val;
            return this;
        }

        public Builder withTeamSize(Integer val) {
            teamSize = val;
            return this;
        }

        public Builder withStartWorkAt(String val) {
            startWorkAt = val;
            return this;
        }

        public Builder withEndWorkAt(String val) {
            endWorkAt = val;
            return this;
        }

        public Builder withPosition(String val) {
            position = val;
            return this;
        }

        public Builder withResponsibilities(String val) {
            responsibilities = val;
            return this;
        }

        public Builder withEmployeeId(Long val) {
            employeeId = val;
            return this;
        }

        public Builder withProjectTechnologies(List<ProjectTechnologyDto> val) {
            projectTechnologies = val;
            return this;
        }

        public ProjectRequestDto build() {
            return new ProjectRequestDto(this);
        }
    }
}
