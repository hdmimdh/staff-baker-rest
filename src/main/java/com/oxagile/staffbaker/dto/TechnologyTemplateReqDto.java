package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.NotBlank;

public class TechnologyTemplateReqDto {

    @NotBlank(message = "title.required.violation")
    private String title;

    @NotBlank(message = "technology.type.required.violation")
    private String type;

    public TechnologyTemplateReqDto() {
    }

    private TechnologyTemplateReqDto(Builder builder) {
        setTitle(builder.title);
        type = builder.type;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static final class Builder {
        private String title;
        private String type;

        private Builder() {
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(String val) {
            type = val;
            return this;
        }

        public TechnologyTemplateReqDto build() {
            return new TechnologyTemplateReqDto(this);
        }
    }
}
