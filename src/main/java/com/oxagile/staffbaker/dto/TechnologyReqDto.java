package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class TechnologyReqDto {

    private Long id;

    @NotBlank(message = "title.required.violation")
    private String title;

    @NotBlank(message = "technology.type.required.violation")
    private String type;

    @NotNull
    private Integer experience;

    @NotBlank
    private String lastTimeUsed;

    @NotNull
    private Long employeeId;

    public TechnologyReqDto() {
    }

    private TechnologyReqDto(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setType(builder.type);
        setExperience(builder.experience);
        setLastTimeUsed(builder.lastTimeUsed);
        setEmployeeId(builder.employeeId);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getLastTimeUsed() {
        return lastTimeUsed;
    }

    public void setLastTimeUsed(String lastTimeUsed) {
        this.lastTimeUsed = lastTimeUsed;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }


    public static final class Builder {
        private String title;
        private String type;
        private Integer experience;
        private String lastTimeUsed;
        private Long employeeId;
        private Long id;

        private Builder() {
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withType(String val) {
            type = val;
            return this;
        }

        public Builder withExperience(Integer val) {
            experience = val;
            return this;
        }

        public Builder withLastTimeUsed(String val) {
            lastTimeUsed = val;
            return this;
        }

        public Builder withEmployeeId(Long val) {
            employeeId = val;
            return this;
        }

        public TechnologyReqDto build() {
            return new TechnologyReqDto(this);
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }
    }
}
