package com.oxagile.staffbaker.dto;

public class DegreeTemplateReqDto {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "DegreeTemplateReqDto{" +
                "title=" + title +
                '}';
    }
}
