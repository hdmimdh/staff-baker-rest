package com.oxagile.staffbaker.dto;

public class PairDto<V, K> {

    private V v;
    private K k;

    public PairDto(V v, K k) {
        this.v = v;
        this.k = k;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }

    public K getK() {
        return k;
    }

    public void setK(K k) {
        this.k = k;
    }
}
