package com.oxagile.staffbaker.dto;


import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

public class ProjectTemplateReqDto {

    @NotBlank(message = "{project.template.title.required.violation}")
    @Size(max = 255, message = "{project.template.title.size.violation}")
    private String title;

    @Size(max = 255, message = "{project.template.title.description.violation}")
    private String description;

    @Size(max = 1000, message = "{project.template.title.summary.violation}")
    private String summary;

    public ProjectTemplateReqDto() {
    }

    private ProjectTemplateReqDto(Builder builder) {
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }


    public static final class Builder {
        private String title;
        private String description;
        private String summary;

        private Builder() {
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public ProjectTemplateReqDto build() {
            return new ProjectTemplateReqDto(this);
        }
    }
}
