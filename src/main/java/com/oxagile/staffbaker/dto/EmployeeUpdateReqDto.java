package com.oxagile.staffbaker.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.oxagile.staffbaker.domain.Department;
import com.oxagile.staffbaker.domain.Gender;
import com.oxagile.staffbaker.dto.converter.ZuluLocalDateDeserializer;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class EmployeeUpdateReqDto {

    @NotBlank(message = "{firstName.required.violation}")
    private String firstName;

    @NotBlank(message = "{lastName.required.violation}")
    private String lastName;

    @NotNull(message = "{department.required.violation}")
    private Department department;

    @NotNull(message = "{gender.required.violation}")
    private Gender gender;

    @NotBlank(message = "{phone.required.violation}")
    private String phone;

    @NotBlank(message = "{skype.required.violation}")
    private String skype;

    private String telegram;

    @NotBlank(message = "{workingPlace.required.violation}")
    private String workingPlace;

    @NotBlank(message = "{title.required.violation}")
    private String title;

    private String pictureUrl;
    private String summary;

    @JsonDeserialize(using = ZuluLocalDateDeserializer.class)
    private LocalDate birthday;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getWorkingPlace() {
        return workingPlace;
    }

    public void setWorkingPlace(String workingPlace) {
        this.workingPlace = workingPlace;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
