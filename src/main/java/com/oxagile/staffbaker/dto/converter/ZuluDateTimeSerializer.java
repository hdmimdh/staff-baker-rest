package com.oxagile.staffbaker.dto.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Zulu standard iso format serializer.
 */
public class ZuluDateTimeSerializer extends JsonSerializer<ZonedDateTime> {

    static final String ISO_INSTANT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSX";

    @Override
    public void serialize(ZonedDateTime value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        gen.writeString(value != null ? value.format(DateTimeFormatter.ofPattern(ISO_INSTANT_FORMAT).withZone(ZoneOffset.UTC)) : "");
    }
}
