package com.oxagile.staffbaker.dto.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static com.oxagile.staffbaker.dto.converter.ZuluDateTimeSerializer.ISO_INSTANT_FORMAT;

/**
 * Zulu standard iso format deserializer.
 */
public class ZuluDateTimeDeserializer extends JsonDeserializer<ZonedDateTime> {

    @Override
    public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return ZonedDateTime.parse(jsonParser.getValueAsString(), DateTimeFormatter.ofPattern(ISO_INSTANT_FORMAT).withZone(ZoneOffset.UTC));
    }
}
