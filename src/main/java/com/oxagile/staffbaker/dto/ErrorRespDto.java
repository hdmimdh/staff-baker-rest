package com.oxagile.staffbaker.dto;

public class ErrorRespDto {

    private String message;
    private int errorCode;

    private ErrorRespDto(Builder builder) {
        setMessage(builder.message);
        setErrorCode(builder.errorCode);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }


    public static final class Builder {
        private String message;
        private int errorCode;

        private Builder() {
        }

        public Builder withMessage(String val) {
            message = val;
            return this;
        }

        public Builder withErrorCode(int val) {
            errorCode = val;
            return this;
        }

        public ErrorRespDto build() {
            return new ErrorRespDto(this);
        }
    }
}
