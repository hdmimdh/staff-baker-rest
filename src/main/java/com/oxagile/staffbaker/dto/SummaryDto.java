package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.NotBlank;

public class SummaryDto {

    @NotBlank(message = "{summary.required.violation}")
    private String summary;

    public SummaryDto() {
    }

    private SummaryDto(Builder builder) {
        setSummary(builder.summary);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }


    public static final class Builder {
        private String summary;

        private Builder() {
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public SummaryDto build() {
            return new SummaryDto(this);
        }
    }
}
