package com.oxagile.staffbaker.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oxagile.staffbaker.dto.converter.ZuluDateTimeSerializer;

import java.time.ZonedDateTime;
import java.util.Objects;

public class ProjectTemplateDto {

    private Long id;
    private String title;
    private String description;
    private String summary;

    private Boolean active;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime createdAt;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime updatedAt;

    @JsonSerialize(using = ZuluDateTimeSerializer.class)
    private ZonedDateTime deactivatedAt;

    private String createdBy;
    private String lastUpdatedBy;

    private ProjectTemplateDto(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setDescription(builder.description);
        setSummary(builder.summary);
        setActive(builder.active);
        setCreatedAt(builder.createdAt);
        setUpdatedAt(builder.updatedAt);
        setDeactivatedAt(builder.deactivatedAt);
        setCreatedBy(builder.createdBy);
        setLastUpdatedBy(builder.lastUpdatedBy);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getDeactivatedAt() {
        return deactivatedAt;
    }

    public void setDeactivatedAt(ZonedDateTime deactivatedAt) {
        this.deactivatedAt = deactivatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectTemplateDto that = (ProjectTemplateDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(active, that.active) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt) &&
                Objects.equals(deactivatedAt, that.deactivatedAt) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(lastUpdatedBy, that.lastUpdatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, summary, active, createdAt, updatedAt, deactivatedAt, createdBy, lastUpdatedBy);
    }


    public static final class Builder {
        private Long id;
        private String title;
        private String description;
        private String summary;
        private Boolean active;
        private ZonedDateTime createdAt;
        private ZonedDateTime updatedAt;
        private ZonedDateTime deactivatedAt;
        private String createdBy;
        private String lastUpdatedBy;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withSummary(String val) {
            summary = val;
            return this;
        }

        public Builder withActive(Boolean val) {
            active = val;
            return this;
        }

        public Builder withCreatedAt(ZonedDateTime val) {
            createdAt = val;
            return this;
        }

        public Builder withUpdatedAt(ZonedDateTime val) {
            updatedAt = val;
            return this;
        }

        public Builder withDeactivatedAt(ZonedDateTime val) {
            deactivatedAt = val;
            return this;
        }

        public Builder withCreatedBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder withLastUpdatedBy(String val) {
            lastUpdatedBy = val;
            return this;
        }

        public ProjectTemplateDto build() {
            return new ProjectTemplateDto(this);
        }
    }
}
