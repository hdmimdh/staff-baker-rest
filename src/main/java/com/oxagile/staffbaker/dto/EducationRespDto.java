package com.oxagile.staffbaker.dto;

public class EducationRespDto {

    private String school;

    private String due;

    private String degree;

    public EducationRespDto() {
    }

    private EducationRespDto(Builder builder) {
        setSchool(builder.school);
        setDue(builder.due);
        setDegree(builder.degree);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }


    public static final class Builder {
        private String school;
        private String due;
        private String degree;

        private Builder() {
        }

        public Builder withSchool(String val) {
            school = val;
            return this;
        }

        public Builder withDue(String val) {
            due = val;
            return this;
        }

        public Builder withDegree(String val) {
            degree = val;
            return this;
        }

        public EducationRespDto build() {
            return new EducationRespDto(this);
        }
    }
}
