package com.oxagile.staffbaker.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class EmployeeReqDto extends EmployeeUpdateReqDto {

    @Email
    @NotBlank(message = "{email.required.violation}")
    private String email;

    @NotBlank(message = "{password.required.violation}")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
