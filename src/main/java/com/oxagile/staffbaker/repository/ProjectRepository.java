package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("SELECT p FROM Project p LEFT JOIN FETCH p.projectTechnologies t WHERE p.employee.id = :employeeId")
    Set<Project> findAllByEmployeeId(@Param("employeeId") Long employeeId);
}
