package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.TechnologyTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnologyTemplateRepository extends JpaRepository<TechnologyTemplate, Long> {

    TechnologyTemplate findByTitle(String title);
}
