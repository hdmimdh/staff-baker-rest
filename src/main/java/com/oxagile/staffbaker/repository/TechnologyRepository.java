package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.Technology;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnologyRepository extends JpaRepository<Technology, Long> {
}
