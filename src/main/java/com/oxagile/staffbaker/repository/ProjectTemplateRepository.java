package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.ProjectTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectTemplateRepository extends JpaRepository<ProjectTemplate, Long> {

    ProjectTemplate findByTitle(String title);
}
