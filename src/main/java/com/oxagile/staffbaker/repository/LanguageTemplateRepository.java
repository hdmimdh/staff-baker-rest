package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.LanguageTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LanguageTemplateRepository extends JpaRepository<LanguageTemplate, Long> {

    List<LanguageTemplate> findAll();
}
