package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language, Long> {
}
