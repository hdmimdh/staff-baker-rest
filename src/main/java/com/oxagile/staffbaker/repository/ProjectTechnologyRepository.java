package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.ProjectTechnology;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectTechnologyRepository extends JpaRepository<ProjectTechnology, Long> {

    void deleteProjectTechnologyByProjectId(Long projectId);
}
