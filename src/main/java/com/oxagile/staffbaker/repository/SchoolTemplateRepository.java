package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.SchoolTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolTemplateRepository extends JpaRepository<SchoolTemplate, Long> {
}
