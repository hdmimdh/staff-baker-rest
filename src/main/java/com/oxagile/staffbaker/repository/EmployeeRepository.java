package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository class for {@link Employee}.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

    Employee findEmployeeByEmail(String email);

    @Query("SELECT e FROM Employee e LEFT JOIN FETCH e.technologies LEFT JOIN FETCH e.languages LEFT JOIN FETCH e.educations WHERE e.email = :email")
    Employee findEmployeeFullByEmail(@Param(value = "email") String email);

    @Query("SELECT e FROM Employee e LEFT JOIN FETCH e.technologies LEFT JOIN FETCH e.languages " +
            "LEFT JOIN FETCH e.educations LEFT JOIN e.projects p LEFT JOIN p.projectTechnologies WHERE e.id = :id")
    Employee findEmployeeFullById(@Param(value = "id") Long id);
}
