package com.oxagile.staffbaker.repository;

import com.oxagile.staffbaker.domain.DegreeTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DegreeTemplateRepository extends JpaRepository<DegreeTemplate, Long> {
}
