package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.session.SessionRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.dto.CredentialsDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.service.AuthService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "REST Service to work with security.")
public class AuthController {
    private static final Logger LOGGER = LogManager.getLogger(AuthController.class);

    private final AuthService authService;
    private final SessionRepository sessionRepository;

    @Autowired
    public AuthController(AuthService authService, SessionRepository sessionRepository) {
        this.authService = authService;
        this.sessionRepository = sessionRepository;
    }

    @ApiOperation(value = "Authenticate an employee.", httpMethod = METHOD_POST)
    @RequestMapping(value = "/login", method = POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    public EmployeeRespDto login(@RequestBody CredentialsDto credentials) {

        LOGGER.info("Employee authentication by {}", credentials);
        return authService.login(credentials);
    }


    @ApiOperation(value = "Logout an employee.", httpMethod = METHOD_POST)
    @RequestMapping(value = "/logout", method = POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    public void logout(HttpServletRequest request, HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {

            SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
            securityContextLogoutHandler.setClearAuthentication(true);
            securityContextLogoutHandler.logout(request, response, auth);
            sessionRepository.delete(request.getRequestedSessionId());
        }

    }
}
