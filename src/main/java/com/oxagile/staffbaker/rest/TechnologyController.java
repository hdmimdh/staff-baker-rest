package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_DELETE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_PUT;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Stopwatch;
import com.oxagile.staffbaker.domain.TechnologyType;
import com.oxagile.staffbaker.dto.PairDto;
import com.oxagile.staffbaker.dto.TechnologyReqDto;
import com.oxagile.staffbaker.service.TechnologyService;
import com.oxagile.staffbaker.utils.security.SecurityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "REST Service to work with technology resource.")
public class TechnologyController {

    private static final Logger LOGGER = LogManager.getLogger(TechnologyController.class);

    private final TechnologyService technologyService;
    private final SecurityUtils securityUtils;

    @Autowired
    public TechnologyController(TechnologyService technologyService, SecurityUtils securityUtils) {
        this.technologyService = technologyService;
        this.securityUtils = securityUtils;
    }

    @ApiOperation(value = "Saves a new technology templates.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology/all", method = RequestMethod.POST)
    public void saveTechnologies(@Valid @RequestBody List<TechnologyReqDto> requestDtos) {

        Stopwatch timer = Stopwatch.createStarted();
        technologyService.saveAll(requestDtos);

        LOGGER.info("saveTechnologies took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    @ApiOperation(value = "Updates a new technology templates.", httpMethod = METHOD_PUT)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology/{employeeId}", method = RequestMethod.PUT)
    public void updateTechnologies(@PathVariable Long employeeId, @Valid @RequestBody List<TechnologyReqDto> requestDtos) {

        Stopwatch timer = Stopwatch.createStarted();

        securityUtils.validateCurrentUser(employeeId, "update technology templates");
        technologyService.updateAll(employeeId, requestDtos);

        LOGGER.info("updateTechnologies took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    @ApiOperation(value = "Deletes technologies.", httpMethod = METHOD_DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology/all", method = RequestMethod.DELETE)
    public void deleteTechnologies(@RequestParam List<Long> ids) {

        Stopwatch timer = Stopwatch.createStarted();
        technologyService.deleteAll(ids);

        LOGGER.info("deleteTechnologies took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    @ApiOperation(value = "Returns all technology types as list.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology-type", method = RequestMethod.GET)
    public List<PairDto> findAllTechnologyTypes() {

        LOGGER.info("Returning all technology types.");
        return TechnologyType.getTechnologies();
    }
}
