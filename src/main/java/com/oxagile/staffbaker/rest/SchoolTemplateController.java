package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.domain.SchoolTemplate;
import com.oxagile.staffbaker.dto.SchoolTemplateReqDto;
import com.oxagile.staffbaker.service.SchoolTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "Rest endpoint for working with school templates.")
public class SchoolTemplateController {

    private static final Logger LOGGER = LogManager.getLogger(SchoolTemplateController.class);

    private final SchoolTemplateService schoolTemplateService;

    @Autowired
    public SchoolTemplateController(SchoolTemplateService schoolTemplateService) {
        this.schoolTemplateService = schoolTemplateService;
    }

    @ApiOperation(value = "Finds all school templates.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/school-template", method = RequestMethod.GET)
    public List<SchoolTemplate> findAllSchoolTemplates() {

        LOGGER.info("Finding all school templates.");
        return schoolTemplateService.findAll();
    }

    @ApiOperation(value = "Saves all school templates.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/school-template/all", method = RequestMethod.POST)
    public void saveSchoolTemplates(@RequestBody List<SchoolTemplateReqDto> reqDtos) {

        LOGGER.info("Saving all school templates.");
        schoolTemplateService.saveAll(reqDtos);
    }

}
