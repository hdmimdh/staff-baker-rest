package com.oxagile.staffbaker.rest.advice;

import com.oxagile.staffbaker.dto.ErrorRespDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorRespDto> handleIllegalArgumentException(IllegalArgumentException e) {

        ErrorRespDto error = ErrorRespDto.newBuilder()
                .withMessage(e.getMessage())
                .withErrorCode(HttpStatus.BAD_REQUEST.value())
                .build();
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
