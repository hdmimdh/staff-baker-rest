package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.domain.DegreeTemplate;
import com.oxagile.staffbaker.dto.DegreeTemplateReqDto;
import com.oxagile.staffbaker.service.DegreeTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "Rest endpoint for working with degree templates.")
public class DegreeTemplateController {

    private static final Logger LOGGER = LogManager.getLogger(DegreeTemplateController.class);

    private final DegreeTemplateService degreeTemplateService;

    @Autowired
    public DegreeTemplateController(DegreeTemplateService degreeTemplateService) {
        this.degreeTemplateService = degreeTemplateService;
    }

    @ApiOperation(value = "Finds all degree templates.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/degree-template", method = RequestMethod.GET)
    public List<DegreeTemplate> findAllDegreeTemplates() {

        LOGGER.info("Finding all degree templates.");
        return degreeTemplateService.findAll();
    }

    @ApiOperation(value = "Saves all degree templates.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/degree-template/all", method = RequestMethod.POST)
    public void saveDegreeTemplates(@RequestBody List<DegreeTemplateReqDto> reqDtos) {

        LOGGER.info("Saving all degree templates.");
        degreeTemplateService.saveAll(reqDtos);
    }
}
