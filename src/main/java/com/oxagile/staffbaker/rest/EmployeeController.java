package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_PUT;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.EmployeeReqDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.dto.EmployeeUpdateReqDto;
import com.oxagile.staffbaker.dto.SummaryDto;
import com.oxagile.staffbaker.service.EmployeeService;
import com.oxagile.staffbaker.service.ExportService;
import com.oxagile.staffbaker.utils.security.SecurityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "REST Service to work with employee resource.")
public class EmployeeController {

    private static final Logger LOGGER = LogManager.getLogger(EmployeeController.class);

    private final EmployeeService employeeService;
    private final ExportService exportService;
    private final SecurityUtils securityUtils;

    @Autowired
    public EmployeeController(EmployeeService employeeService, ExportService exportService, SecurityUtils securityUtils) {
        this.employeeService = employeeService;
        this.exportService = exportService;
        this.securityUtils = securityUtils;
    }

    @ApiOperation(value = "Saves a new employee.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public Long saveEmployee(@Valid @RequestBody EmployeeReqDto employeeReqDto) {

        LOGGER.info("Adding new employee of email - {}", employeeReqDto.getEmail());
        return employeeService.save(employeeReqDto);
    }

    @ApiOperation(value = "Finds employee by id.", httpMethod = METHOD_GET, response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public EmployeeRespDto findEmployeeById(@PathVariable(value = "id") Long id) {

        LOGGER.info("Finding employees by id of - {}", id);
        return employeeService.find(id);
    }

    @ApiOperation(value = "Finds all employees.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public List<EmployeeRespDto> findAllEmployees() {

        LOGGER.info("Finding all employees.");
        return employeeService.findAll();
    }

    @ApiOperation(value = "Updates the whole employee.", httpMethod = METHOD_PUT)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee/{id}", method = RequestMethod.PUT)
    public void updateEmployee(@PathVariable(value = "id") Long id, @Valid @RequestBody EmployeeUpdateReqDto employeeRequestDto) {

        LOGGER.info("Updating employee with id of - {}", id);
        employeeService.update(id, employeeRequestDto);
    }

    @ApiOperation(value = "Updates employees summary.", httpMethod = METHOD_PUT)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee/{id}/summary", method = RequestMethod.PUT)
    public void updateEmployeesSummary(@PathVariable(value = "id") Long id, @Valid @RequestBody SummaryDto summaryDto) {

        LOGGER.info("Updating summary of employee with id of - {}", id);

        securityUtils.validateCurrentUser(id, "update summary of employee");
        employeeService.updateSummary(id, summaryDto.getSummary());
    }

    @ApiOperation(value = "Deactivates employee.", httpMethod = METHOD_PUT)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee/{id}/deactivate", method = RequestMethod.PUT)
    public void deactivateEmployee(@PathVariable(value = "id") Long id) {

        LOGGER.info("Deactivate employee with id of - {}", id);
        employeeService.deactivate(id);
    }

    @ApiOperation(value = "Searches for employees.", httpMethod = METHOD_GET, response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "Number of a page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "Number of records within the page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "Sorting string", dataType = "string", paramType = "query", example = "?sort=firstName,asc&sort=lastName,desc")

    })
    @RequestMapping(value = "/employee/search", method = RequestMethod.GET)
    public Page<EmployeeRespDto> searchEmployees(@RequestParam(value = "query", required = false) String query,
                                                 @PageableDefault(value = 20) @ApiIgnore Pageable pageable) {

        LOGGER.info("Search for employees by pageable of - {} and query of - {}", pageable, query);
        if (StringUtils.isBlank(query)) {
            return employeeService.findAll(pageable);
        }
        return employeeService.findAll(query, pageable);
    }

    @ApiOperation(value = "Exports an employee profile as word document.", httpMethod = METHOD_GET, response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/employee/{id}/word", method = RequestMethod.GET)
    public ResponseEntity<ByteArrayResource> exportWord(@PathVariable(value = "id") Long id) {

        LOGGER.info("Exporting employee profile with id of - {} as word document ", id);
        return exportService.exportWord(id);
    }
}
