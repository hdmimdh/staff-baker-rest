package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_PUT;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.ProjectTemplateDto;
import com.oxagile.staffbaker.dto.ProjectTemplateReqDto;
import com.oxagile.staffbaker.service.ProjectTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/project-template")
@Api(value = "/api/v1/project-template", description = "Rest endpoint for working with project templates.")
public class ProjectTemplateController {

    private static final Logger LOGGER = LogManager.getLogger(ProjectTemplateController.class);

    private final ProjectTemplateService projectTemplateService;

    @Autowired
    public ProjectTemplateController(ProjectTemplateService projectTemplateService) {
        this.projectTemplateService = projectTemplateService;
    }

    @ApiOperation(value = "Saves a new project template.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(method = RequestMethod.POST)
    public Long saveProjectTemplate(@Valid @RequestBody ProjectTemplateReqDto requestDto) {

        LOGGER.info("Adding new project template with name of {}." + requestDto.getTitle());
        return projectTemplateService.save(requestDto);
    }

    @ApiOperation(value = "Finds project template project by id.", httpMethod = METHOD_GET, response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ProjectTemplateDto findProjectTemplate(@PathVariable(value = "id") Long id) {

        LOGGER.info("Searching project template by id {}.", id);
        return projectTemplateService.find(id);
    }

    @ApiOperation(value = "Finds all project templates.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectTemplateDto> findProjectTemplates() {

        LOGGER.info("Finding all project templates.");
        return projectTemplateService.findAll();
    }

    @ApiOperation(value = "Updates a project template of the given id.", httpMethod = METHOD_PUT, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateProjectTemplate(@PathVariable(value = "id") Long id,
                                      @Valid @RequestBody ProjectTemplateReqDto requestDto) {

        LOGGER.info("Updating a project template with id of {}.", id);
        projectTemplateService.update(id, requestDto);
    }

    @ApiOperation(value = "Removes project template of the given id.", httpMethod = METHOD_PUT, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/{id}/deactivate", method = RequestMethod.PUT)
    public void deactivateProjectTemplate(@PathVariable(value = "id") Long id) {

        LOGGER.info("Deactivating of the project template with id {}.", id);
        projectTemplateService.deactivate(id);
    }
}
