package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_DELETE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_PUT;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Stopwatch;
import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.ProjectRequestDto;
import com.oxagile.staffbaker.dto.ProjectResponseDto;
import com.oxagile.staffbaker.service.ProjectService;
import com.oxagile.staffbaker.utils.security.SecurityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1/project")
@Api(value = "/api/v1/project", description = "Rest endpoint for working with employee projects.")
public class ProjectController {

    private static final Logger LOGGER = LogManager.getLogger(ProjectController.class);

    private final ProjectService projectService;
    private final SecurityUtils securityUtils;

    @Autowired
    public ProjectController(ProjectService projectService, SecurityUtils securityUtils) {
        this.projectService = projectService;
        this.securityUtils = securityUtils;
    }

    @ApiOperation(value = "Saves a project.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(method = RequestMethod.POST)
    public void saveProject(@Valid @RequestBody ProjectRequestDto requestDto) {

        Stopwatch timer = Stopwatch.createStarted();
        projectService.save(requestDto);

        LOGGER.info("saveProject took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    @ApiOperation(value = "Updates a project.", httpMethod = METHOD_PUT)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateProject(@PathVariable(value = "id") Long id, @Valid @RequestBody ProjectRequestDto requestDto) {

        Stopwatch timer = Stopwatch.createStarted();

        securityUtils.validateCurrentUser(requestDto.getEmployeeId(), "update a project");
        projectService.update(id, requestDto);

        LOGGER.info("updateProject took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    @ApiOperation(value = "Finds project by id.", httpMethod = METHOD_GET, response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectResponseDto> findProjectByEmployee(@RequestParam("employeeId") Long employeeId) {

        LOGGER.info("Searching project for employee with id {}.", employeeId);
        return projectService.findByEmployee(employeeId);
    }

    @ApiOperation(value = "Deletes a project.", httpMethod = METHOD_DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteProject(@PathVariable(value = "id") Long id) {

        Stopwatch timer = Stopwatch.createStarted();
        projectService.delete(id);

        LOGGER.info("deleteProject took {} ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
    }
}
