package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.BAD_REQUEST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_DELETE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_PUT;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.dto.TechnologyTemplateDto;
import com.oxagile.staffbaker.dto.TechnologyTemplateReqDto;
import com.oxagile.staffbaker.service.TechnologyTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "REST Service to work with technology template resource.")
public class TechnologyTemplateController {

    private static final Logger LOGGER = LogManager.getLogger(TechnologyTemplateController.class);

    private final TechnologyTemplateService technologyTemplateService;

    @Autowired
    public TechnologyTemplateController(TechnologyTemplateService technologyTemplateService) {
        this.technologyTemplateService = technologyTemplateService;
    }

    @ApiOperation(value = "Saves a new technology template.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology-template", method = RequestMethod.POST)
    public Long saveTechnologyTemplate(@Valid @RequestBody TechnologyTemplateReqDto requestDto) {

        LOGGER.info("Adding new technology template {}", requestDto.getTitle());
        return technologyTemplateService.save(requestDto);
    }

    @ApiOperation(value = "Finds all technology templates.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology-template", method = RequestMethod.GET)
    public List<TechnologyTemplateDto> findAllTechnologyTemplates() {

        LOGGER.info("Finding all technology templates.");
        return technologyTemplateService.findAll();
    }

    @ApiOperation(value = "Updates the technology template.", httpMethod = METHOD_PUT, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology-template/{id}", method = RequestMethod.PUT)
    public void updateTechnologyTemplate(@PathVariable(value = "id") Long id, @Valid @RequestBody TechnologyTemplateReqDto requestDto) {

        LOGGER.info("Updating technology template with id of - {}", id);
        technologyTemplateService.update(id, requestDto);
    }

    @ApiOperation(value = "Deletes the technology template.", httpMethod = METHOD_DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/technology-template/{id}", method = RequestMethod.DELETE)
    public void deleteTechnologyTemplate(@PathVariable(value = "id") Long id) {

        LOGGER.info("Deleting technology template by id of - {}", id);
        technologyTemplateService.delete(id);
    }
}
