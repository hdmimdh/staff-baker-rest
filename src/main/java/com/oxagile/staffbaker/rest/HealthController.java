package com.oxagile.staffbaker.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;


@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "REST Service to work with health resource.")
public class HealthController {

    @ApiOperation(value = "Verifies the health check of the service.", nickname = "health")
    @RequestMapping(value = "/health", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    public String verifyHealth() {
        return "Healthy";
    }
}
