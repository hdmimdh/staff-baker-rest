package com.oxagile.staffbaker.rest;

import static com.oxagile.staffbaker.utils.SwaggerConstants.INTERNAL_SERVER_ERROR;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_GET;
import static com.oxagile.staffbaker.utils.SwaggerConstants.METHOD_POST;
import static com.oxagile.staffbaker.utils.SwaggerConstants.OK_RESPONSE;
import static com.oxagile.staffbaker.utils.SwaggerConstants.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oxagile.staffbaker.domain.LanguageTemplate;
import com.oxagile.staffbaker.dto.LanguageTemplateReqDto;
import com.oxagile.staffbaker.service.LanguageTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "Rest endpoint for working with language templates.")
public class LanguageTemplateController {

    private static final Logger LOGGER = LogManager.getLogger(LanguageTemplateController.class);

    private final LanguageTemplateService languageTemplateService;

    @Autowired
    public LanguageTemplateController(LanguageTemplateService languageTemplateService) {
        this.languageTemplateService = languageTemplateService;
    }

    @ApiOperation(value = "Finds all language templates.", httpMethod = METHOD_GET, response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/language-template", method = RequestMethod.GET)
    public List<LanguageTemplate> findAllLanguageTemplates() {

        LOGGER.info("Finding all language templates.");
        return languageTemplateService.findAll();
    }

    @ApiOperation(value = "Saves all language templates.", httpMethod = METHOD_POST)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = OK_RESPONSE, response = String.class),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED, response = String.class),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/language-template/all", method = RequestMethod.POST)
    public void saveLanguageTemplates(@RequestBody List<LanguageTemplateReqDto> reqDtos) {

        LOGGER.info("Saving all language templates.");
        languageTemplateService.saveAll(reqDtos);
    }

}
