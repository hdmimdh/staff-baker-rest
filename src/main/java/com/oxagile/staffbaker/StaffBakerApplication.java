package com.oxagile.staffbaker;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

/**
 * StaffBakerApplication runner.
 */
@SpringBootApplication
@PropertySource({
        "classpath:database.properties"
})
@EnableJpaAuditing
@EnableSpringDataWebSupport
@EnableJpaRepositories(basePackages = {"com.oxagile.staffbaker.repository"})
@EntityScan(basePackages = {"com.oxagile.staffbaker.domain"})
public class StaffBakerApplication {

    private static final Logger LOGGER = LogManager.getLogger(StaffBakerApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        Environment env = SpringApplication.run(StaffBakerApplication.class, args).getEnvironment();

        LOGGER.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}\n\t" +
                        "External: \thttp://{}:{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port") + "/swagger-ui.html#/",
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port") + "/swagger-ui.html#/");
    }
}
