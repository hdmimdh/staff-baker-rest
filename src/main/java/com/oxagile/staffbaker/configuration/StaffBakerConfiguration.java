package com.oxagile.staffbaker.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.oxagile.staffbaker.configuration.jpa.UsernameAuditorAware;
import com.oxagile.staffbaker.service.converter.EmployeeFromDtoConverter;
import com.oxagile.staffbaker.service.converter.EmployeeToResponseDtoConverter;
import com.oxagile.staffbaker.service.converter.ProjectTemplateFromDtoConverter;
import com.oxagile.staffbaker.service.converter.ProjectTemplateToResponseDtoConverter;
import com.oxagile.staffbaker.service.converter.TechnologyTemplateToDtoConverter;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.AuditorAware;


@Configuration
public class StaffBakerConfiguration {

    @Bean
    public ConversionService conversionService(PasswordEncoder passwordEncoder) {

        ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
        bean.setConverters(Sets.newHashSet(
                new EmployeeToResponseDtoConverter(),
                new EmployeeFromDtoConverter(passwordEncoder),
                new ProjectTemplateToResponseDtoConverter(),
                new ProjectTemplateFromDtoConverter(),
                new TechnologyTemplateToDtoConverter(),
                new SerializingConverter(),
                new DeserializingConverter()
        ));
        bean.afterPropertiesSet();
        return bean.getObject();
    }

    @Bean
    public ObjectMapper objectMapper() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return mapper;
    }

    @Primary
    @Bean
    AuditorAware<String> auditorProvider() {
        return new UsernameAuditorAware();
    }
}
