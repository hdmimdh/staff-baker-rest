package com.oxagile.staffbaker.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

import com.oxagile.staffbaker.domain.Roles;
import com.oxagile.staffbaker.utils.security.UnauthorizedEntryPoint;
import com.oxagile.staffbaker.web.filter.CorsFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationProvider authenticationProvider;

    @Autowired
    public WebSecurityConfig(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @Bean
    public UnauthorizedEntryPoint unauthorizedEntryPoint() {
        return new UnauthorizedEntryPoint();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(
                        "/v2/api-docs",
                        "/swagger-resources/configuration/ui",
                        "/swagger-resources/configuration/security",
                        "/swagger-ui.html",
                        "/swagger-resources",
                        "/webjars/**"
                );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf()
                .disable()

                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedEntryPoint())

                .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()

                .antMatchers(HttpMethod.POST, "/api/v1/login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/employee").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.PUT, "/api/v1/employee/{\\d+}").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.PUT, "/api/v1/employee/{\\d+}/deactivate").hasAuthority(Roles.ADMIN.toString())

                .antMatchers(HttpMethod.POST, "/api/v1/project-template").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.PUT, "/api/v1/project-template/{\\d+}").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.PUT, "/api/v1/project-template/{\\d+}/deactivate").hasAuthority(Roles.ADMIN.toString())

                .antMatchers(HttpMethod.POST, "/api/v1/technology-template").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.PUT, "/api/v1/technology-template/{\\d+}").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.DELETE, "/api/v1/technology-template/{\\d+}").hasAuthority(Roles.ADMIN.toString())

                .antMatchers(HttpMethod.POST, "/api/v1/degree-template/**").hasAuthority(Roles.ADMIN.toString())
                .antMatchers(HttpMethod.POST, "/api/v1/school-template/**").hasAuthority(Roles.ADMIN.toString())

                .anyRequest().authenticated()
                .and()

                .addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class);
    }
}
