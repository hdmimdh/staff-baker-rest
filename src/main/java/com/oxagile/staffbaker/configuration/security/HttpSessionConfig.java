package com.oxagile.staffbaker.configuration.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

@Configuration
@EnableJdbcHttpSession(maxInactiveIntervalInSeconds = 86400)
public class HttpSessionConfig extends AbstractHttpSessionApplicationInitializer {

    private static final String AUTH_TOKEN_HEADER = "Authorization";

    @Bean
    public HttpSessionStrategy httpSessionStrategy() {
        HeaderHttpSessionStrategy headerHttpSessionStrategy = new HeaderHttpSessionStrategy();
        headerHttpSessionStrategy.setHeaderName(AUTH_TOKEN_HEADER);
        return headerHttpSessionStrategy;
    }

}