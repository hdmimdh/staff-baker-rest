package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.domain.DegreeTemplate;
import com.oxagile.staffbaker.dto.DegreeTemplateReqDto;

import java.util.List;

public interface DegreeTemplateService {

    void saveAll(List<DegreeTemplateReqDto> reqDtos);

    List<DegreeTemplate> findAll();
}
