package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.TechnologyReqDto;

import java.util.List;

public interface TechnologyService {

    void saveAll(List<TechnologyReqDto> technologies);

    void updateAll(Long employeeId, List<TechnologyReqDto> technologies);

    void deleteAll(List<Long> ids);
}
