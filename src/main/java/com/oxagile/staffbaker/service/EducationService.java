package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.EducationReqDto;

import java.util.List;

public interface EducationService {

    void saveAll(List<EducationReqDto> reqDtos);
}
