package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.EmployeeUpdateReqDto;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDtoToEmployeeUpdater {

    public Employee convert(EmployeeUpdateReqDto employeeRequestDto, Employee employee) {

        if (employeeRequestDto == null || employee == null) {
            return null;
        }

        employee.setFirstName(employeeRequestDto.getFirstName());
        employee.setLastName(employeeRequestDto.getLastName());
        employee.setDepartment(employeeRequestDto.getDepartment());
        employee.setGender(employeeRequestDto.getGender());
        employee.setPhone(employeeRequestDto.getPhone());
        employee.setSkype(employeeRequestDto.getSkype());
        employee.setTelegram(employeeRequestDto.getTelegram());
        employee.setWorkingPlace(employeeRequestDto.getWorkingPlace());
        employee.setTitle(employeeRequestDto.getTitle());
        employee.setPictureUrl(employeeRequestDto.getPictureUrl());
        employee.setSummary(employeeRequestDto.getSummary());
        employee.setBirthday(employeeRequestDto.getBirthday());

        return employee;
    }
}
