package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Project;
import com.oxagile.staffbaker.domain.ProjectTechnology;
import com.oxagile.staffbaker.dto.ProjectTechnologyDto;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectTechnologyDtoConverter {

    public List<ProjectTechnology> convert(Long projectId, List<ProjectTechnologyDto> projectTechnologies) {

        if (CollectionUtils.isEmpty(projectTechnologies)) {
            return Collections.emptyList();
        }

        return projectTechnologies.stream()
                .map(
                        technology -> ProjectTechnology.newBuilder()
                                .withTitle(technology.getTitle())
                                .withType(technology.getType())
                                .withProject(Project.newBuilder().withId(projectId).build())
                                .build())
                .collect(Collectors.toList());
    }

    public List<ProjectTechnologyDto> convert(List<ProjectTechnology> projectTechnologies) {

        if (CollectionUtils.isEmpty(projectTechnologies)) {
            return Collections.emptyList();
        }

        return projectTechnologies.stream()
                .map(
                        technology -> ProjectTechnologyDto.newBuilder()
                                .withTitle(technology.getTitle())
                                .withType(technology.getType())
                                .build())
                .collect(Collectors.toList());
    }
}
