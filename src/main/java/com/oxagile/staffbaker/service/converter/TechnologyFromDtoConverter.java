package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Technology;
import com.oxagile.staffbaker.domain.TechnologyType;
import com.oxagile.staffbaker.dto.TechnologyReqDto;
import com.oxagile.staffbaker.utils.common.ZonedDateTimeUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TechnologyFromDtoConverter {

    private final ZonedDateTimeUtils zonedDateTimeUtils;

    @Autowired
    public TechnologyFromDtoConverter(ZonedDateTimeUtils zonedDateTimeUtils) {
        this.zonedDateTimeUtils = zonedDateTimeUtils;
    }

    public List<Technology> convert(List<TechnologyReqDto> dtos) {

        if (CollectionUtils.isEmpty(dtos)) {
            return Collections.emptyList();
        }

        return dtos.stream()
                .map(dto -> Technology.newBuilder()
                        .withTitle(dto.getTitle())
                        .withType(EnumUtils.getEnum(TechnologyType.class, dto.getType()))
                        .withExperience(dto.getExperience())
                        .withLastTimeUsed(zonedDateTimeUtils.toEpochMilli(dto.getLastTimeUsed()))
                        .withEmployee(Employee.newBuilder().withId(dto.getEmployeeId()).build())
                        .build())
                .collect(Collectors.toList());
    }

    public List<Technology> convert(Long employeeId, List<TechnologyReqDto> dtos) {

        if (CollectionUtils.isEmpty(dtos)) {
            return Collections.emptyList();
        }

        return dtos.stream()
                .map(dto -> Technology.newBuilder()
                        .withId(dto.getId())
                        .withTitle(dto.getTitle())
                        .withType(EnumUtils.getEnum(TechnologyType.class, dto.getType()))
                        .withExperience(dto.getExperience())
                        .withLastTimeUsed(zonedDateTimeUtils.toEpochMilli(dto.getLastTimeUsed()))
                        .withEmployee(Employee.newBuilder().withId(employeeId).build())
                        .build())
                .collect(Collectors.toList());
    }
}
