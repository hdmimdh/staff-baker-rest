package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.ProjectTemplate;
import com.oxagile.staffbaker.dto.ProjectTemplateDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProjectTemplateToResponseDtoConverter implements Converter<ProjectTemplate, ProjectTemplateDto> {

    @Override
    public ProjectTemplateDto convert(ProjectTemplate projectTemplate) {

        return ProjectTemplateDto.newBuilder()
                .withId(projectTemplate.getId())
                .withTitle(projectTemplate.getTitle())
                .withDescription(projectTemplate.getDescription())
                .withSummary(projectTemplate.getSummary())
                .withActive(projectTemplate.isActive())
                .withCreatedAt(projectTemplate.getCreatedAt())
                .withUpdatedAt(projectTemplate.getUpdatedAt())
                .withDeactivatedAt(projectTemplate.getDeactivatedAt())
                .withCreatedBy(projectTemplate.getCreatedBy())
                .withLastUpdatedBy(projectTemplate.getUpdatedBy())
                .build();
    }
}
