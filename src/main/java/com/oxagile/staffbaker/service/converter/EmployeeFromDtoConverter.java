package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Roles;
import com.oxagile.staffbaker.dto.EmployeeReqDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class EmployeeFromDtoConverter implements Converter<EmployeeReqDto, Employee> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public EmployeeFromDtoConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Employee convert(EmployeeReqDto employeeReqDto) {

        if (employeeReqDto == null) {
            return null;
        }

        return Employee.newBuilder()
                .withEmail(employeeReqDto.getEmail())
                .withPassword(passwordEncoder.encode(employeeReqDto.getPassword()))
                .withRoles(Roles.USER.toString())
                .withFirstName(employeeReqDto.getFirstName())
                .withLastName(employeeReqDto.getLastName())
                .withDepartment(employeeReqDto.getDepartment())
                .withGender(employeeReqDto.getGender())
                .withPhone(employeeReqDto.getPhone())
                .withSkype(employeeReqDto.getSkype())
                .withTelegram(employeeReqDto.getTelegram())
                .withWorkingPlace(employeeReqDto.getWorkingPlace())
                .withTitle(employeeReqDto.getTitle())
                .withPictureUrl(employeeReqDto.getPictureUrl())
                .withSummary(employeeReqDto.getSummary())
                .withBirthday(employeeReqDto.getBirthday())
                .build();
    }

}
