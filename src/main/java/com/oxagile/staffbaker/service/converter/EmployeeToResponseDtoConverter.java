package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Education;
import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Language;
import com.oxagile.staffbaker.dto.EducationRespDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.dto.LanguageDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EmployeeToResponseDtoConverter implements Converter<Employee, EmployeeRespDto> {

    @Override
    public EmployeeRespDto convert(Employee employee) {

        if (employee == null) {
            return null;
        }

        return EmployeeRespDto.newBuilder()
                .withId(employee.getId())
                .withEmail(employee.getEmail())
                .withFirstName(employee.getFirstName())
                .withLastName(employee.getLastName())
                .withDepartment(employee.getDepartment())
                .withGender(employee.getGender())
                .withPhone(employee.getPhone())
                .withSkype(employee.getSkype())
                .withTelegram(employee.getTelegram())
                .withWorkingPlace(employee.getWorkingPlace())
                .withTitle(employee.getTitle())
                .withPictureUrl(employee.getPictureUrl())
                .withSummary(employee.getSummary())
                .withBirthday(employee.getBirthday())
                .withActive(employee.isActive())
                .withCreatedAt(employee.getCreatedAt())
                .withUpdatedAt(employee.getUpdatedAt())
                .withDeactivatedAt(employee.getDeactivatedAt())
                .withCreatedBy(employee.getCreatedBy())
                .withLastUpdatedBy(employee.getUpdatedBy())
                .withLanguages(languages(employee.getLanguages()))
                .withEducations(educations(employee.getEducations()))
                .build();
    }

    private List<LanguageDto> languages(Set<Language> languages) {

        if (CollectionUtils.isEmpty(languages)) {
            return Collections.emptyList();
        }

        return languages.stream()
                .map(
                        language -> LanguageDto.newBuilder()
                                .withTitle(language.getTitle())
                                .withLanguageLevel(language.getLanguageLevel().getDescription())
                                .build())
                .collect(Collectors.toList());
    }

    private List<EducationRespDto> educations(Set<Education> educations) {

        if (CollectionUtils.isEmpty(educations)) {
            return Collections.emptyList();
        }

        return educations.stream()
                .map(
                        education -> EducationRespDto.newBuilder()
                                .withSchool(education.getSchool())
                                .withDegree(education.getDegree())
                                .withDue(due(education.getStartedAt(), education.getEndedAt()))
                                .build())
                .collect(Collectors.toList());
    }

    private String due(LocalDate startDateTime, LocalDate endDateTime) {

        if (startDateTime == null) {
            return "N/A";
        }

        String start = String.valueOf(startDateTime.getYear());
        String end = endDateTime != null ? String.valueOf(endDateTime.getYear()) : "Present";

        return start + " - " + end;
    }
}
