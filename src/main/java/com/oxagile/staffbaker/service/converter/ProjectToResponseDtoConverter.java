package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Project;
import com.oxagile.staffbaker.dto.ProjectResponseDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ProjectToResponseDtoConverter {

    private final ProjectTechnologyDtoConverter projectTechnologyDtoConverter;

    @Autowired
    public ProjectToResponseDtoConverter(ProjectTechnologyDtoConverter projectTechnologyDtoConverter) {
        this.projectTechnologyDtoConverter = projectTechnologyDtoConverter;
    }

    public List<ProjectResponseDto> convert(Set<Project> projects) {

        if (CollectionUtils.isEmpty(projects)) {
            return Collections.emptyList();
        }

        return projects.stream()
                .map(
                        project -> ProjectResponseDto.newBuilder()
                                .withId(project.getId())
                                .withTitle(project.getTitle())
                                .withDescription(project.getDescription())
                                .withSummary(project.getSummary())
                                .withCompany(project.getCompany())
                                .withPosition(project.getPosition())
                                .withTeamSize(project.getTeamSize())
                                .withResponsibilities(project.getResponsibilities())
                                .withStartWorkAt(project.getStartWorkAt())
                                .withEndWorkAt(project.getEndWorkAt())
                                .withProjectTechnologies(projectTechnologyDtoConverter.convert(project.getProjectTechnologies()))
                                .build())
                .collect(Collectors.toList());
    }
}
