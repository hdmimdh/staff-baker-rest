package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.ProjectTemplate;
import com.oxagile.staffbaker.dto.ProjectTemplateReqDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProjectTemplateFromDtoConverter implements Converter<ProjectTemplateReqDto, ProjectTemplate> {

    @Override
    public ProjectTemplate convert(ProjectTemplateReqDto requestDto) {

        return ProjectTemplate.newBuilder()
                .withTitle(requestDto.getTitle())
                .withDescription(requestDto.getDescription())
                .withSummary(requestDto.getSummary())
                .withActive(Boolean.TRUE)
                .build();
    }

}
