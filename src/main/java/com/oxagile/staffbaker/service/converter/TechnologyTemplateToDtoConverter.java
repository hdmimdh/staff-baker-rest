package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.TechnologyTemplate;
import com.oxagile.staffbaker.dto.TechnologyTemplateDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TechnologyTemplateToDtoConverter implements Converter<TechnologyTemplate, TechnologyTemplateDto> {

    @Override
    public TechnologyTemplateDto convert(TechnologyTemplate template) {

        return TechnologyTemplateDto.newBuilder()
                .withId(template.getId())
                .withTitle(template.getTitle())
                .withType(template.getType().name())
                .withDescription(template.getType().getDescription())
                .build();
    }
}
