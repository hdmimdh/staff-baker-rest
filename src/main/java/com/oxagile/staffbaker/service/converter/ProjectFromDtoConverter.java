package com.oxagile.staffbaker.service.converter;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Project;
import com.oxagile.staffbaker.domain.ProjectTechnology;
import com.oxagile.staffbaker.dto.ProjectRequestDto;
import com.oxagile.staffbaker.utils.common.ZonedDateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectFromDtoConverter {

    private final ZonedDateTimeUtils timeUtils;

    private final ProjectTechnologyDtoConverter technologyDtoConverter;

    @Autowired
    public ProjectFromDtoConverter(ZonedDateTimeUtils timeUtils, ProjectTechnologyDtoConverter technologyDtoConverter) {
        this.timeUtils = timeUtils;
        this.technologyDtoConverter = technologyDtoConverter;
    }

    public Project convert(ProjectRequestDto dto) {
        return Project.newBuilder()
                .withTitle(dto.getTitle())
                .withDescription(dto.getDescription())
                .withSummary(dto.getSummary())
                .withCompany(dto.getCompany())
                .withPosition(dto.getPosition())
                .withTeamSize(dto.getTeamSize())
                .withResponsibilities(dto.getResponsibilities())
                .withStartWorkAt(timeUtils.toZonedDateTime(dto.getStartWorkAt()))
                .withEndWorkAt(timeUtils.toZonedDateTime(dto.getEndWorkAt()))
                .withEmployee(Employee.newBuilder().withId(dto.getEmployeeId()).build())
                .build();
    }

    public Project convertWithTechnologies(Long id, ProjectRequestDto dto) {

        Project project = convert(dto);
        project.setId(id);

        List<ProjectTechnology> projectTechnologies = technologyDtoConverter.convert(id, dto.getProjectTechnologies());
        project.setProjectTechnologies(projectTechnologies);

        return project;
    }
}