package com.oxagile.staffbaker.service.export;

import com.oxagile.staffbaker.domain.Education;
import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Language;
import com.oxagile.staffbaker.domain.Project;
import com.oxagile.staffbaker.domain.ProjectTechnology;
import com.oxagile.staffbaker.domain.Technology;
import com.oxagile.staffbaker.domain.TechnologyType;
import com.oxagile.staffbaker.utils.common.ZonedDateTimeUtils;
import com.oxagile.staffbaker.utils.export.ExportPlaceholder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.BodyElementType;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class WordExportService {

    private static final String WORD_TEMPLATE = "src/main/resources/tplt/word_tplt.docx";

    private BigInteger numID;

    private ZonedDateTimeUtils timeUtils;

    @Autowired
    public WordExportService(ZonedDateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    public XWPFDocument export(Employee employee) throws InvalidFormatException, IOException {

        if (employee == null) {
            return null;
        }

        Map<String, String> placeholders = new HashMap<>();

        placeholders.put(ExportPlaceholder.USERNAME_PLC_HOLDER, employee.getFirstName() + " " + employee.getLastName().substring(0, 1));
        placeholders.put(ExportPlaceholder.TITLE_PLC_HOLDER, employee.getTitle());

        XWPFDocument document = new XWPFDocument(OPCPackage.open(WORD_TEMPLATE));

        replace(document, placeholders);
        replaceSummary(document, employee.getSummary());
        replaceTechnologies(document, employee.getTechnologies());
        replaceLanguages(document, new ArrayList<>(employee.getLanguages()));
        replaceEducations(document, new ArrayList<>(employee.getEducations()));
        replaceProjects(document, new ArrayList<>(employee.getProjects()));

        return document;
    }

    private void replaceSummary(XWPFDocument doc, String summary) {

        List<String> rows = Arrays.asList(summary.split(";"));
        for (IBodyElement element : doc.getBodyElements()) {

            if (element.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {

                XWPFParagraph paragraph = (XWPFParagraph) element;
                if (CollectionUtils.isNotEmpty(paragraph.getRuns())
                        && paragraph.getRuns().get(0).getText(paragraph.getRuns().get(0).getTextPosition()).equals(ExportPlaceholder.SUMMARY_PLC_HOLDER)) {

                    paragraph.removeRun(0);

                    XWPFTable table = paragraph.getBody().insertNewTbl(paragraph.getCTP().newCursor());
                    table.getCTTbl().getTblPr().unsetTblBorders();
                    setTableAlignment(table, STJc.Enum.forInt(1));

                    CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
                    cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

                    XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

                    XWPFNumbering numbering = doc.createNumbering();
                    BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);
                    numID = numbering.addNum(abstractNumID);

                    XWPFTableRow xwpfTableRow = table.getRow(0);
                    xwpfTableRow.addNewTableCell();
                    xwpfTableRow.addNewTableCell();
                    xwpfTableRow.addNewTableCell();
                    xwpfTableRow.addNewTableCell();

                    for (String row : rows) {

                        XWPFTableRow lnewRow = table.createRow();
                        XWPFTableCell lnewCell = lnewRow.getCell(4);
                        XWPFParagraph lnewPara = lnewCell.getParagraphs().get(0);
                        lnewPara.setNumID(numID);

                        XWPFRun lnewRun = lnewPara.createRun();
                        lnewRun.setText(row);
                        lnewRun.setColor("777880");
                    }
                    break;
                }
            }
        }
    }

    private void replaceTechnologies(XWPFDocument doc, Set<Technology> technologies) {

        for (IBodyElement element : doc.getBodyElements()) {

            if (element.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {

                XWPFParagraph paragraph = (XWPFParagraph) element;
                if (CollectionUtils.isNotEmpty(paragraph.getRuns())
                        && paragraph.getRuns().get(0).getText(paragraph.getRuns().get(0).getTextPosition()).equals(ExportPlaceholder.TECHNOLOGIES_PLC_HOLDER)) {

                    paragraph.removeRun(0);

                    Map<TechnologyType, List<Technology>> technologiesMap = map(technologies);

                    XWPFTable table = paragraph.getBody().insertNewTbl(paragraph.getCTP().newCursor());
                    table.getCTTbl().getTblPr().unsetTblBorders();


                    setTableAlignment(table, STJc.Enum.forInt(2));

                    CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
                    width.setType(STTblWidth.DXA);
                    width.setW(BigInteger.valueOf(9970));


                    XWPFTableRow row1 = table.getRow(0);
                    row1.addNewTableCell();
                    row1.addNewTableCell();

                    XWPFTableRow row2 = table.createRow();

                    XWPFTableCell cell1 = row2.getCell(0);
                    XWPFRun run1 = cell1.getParagraphs().get(0).createRun();

                    XWPFTableCell cell2 = row2.getCell(1);
                    XWPFRun run2 = cell2.getParagraphs().get(0).createRun();

                    XWPFTableCell cell3 = row2.getCell(2);
                    XWPFRun run3 = cell3.getParagraphs().get(0).createRun();

                    run1.setText("Technology, Tool, Skill");
                    run1.setColor("777880");
                    run1.setItalic(true);

                    run2.setText("Experience (years)");
                    run2.setColor("777880");
                    run2.setItalic(true);

                    run3.setText("Last time used");
                    run3.setColor("777880");
                    run3.setItalic(true);

                    for (Map.Entry<TechnologyType, List<Technology>> entry : technologiesMap.entrySet()) {

                        XWPFTableRow lnewRow = table.createRow();

                        XWPFTableCell lnewCell = lnewRow.getCell(0);

                        lnewCell.setColor("DCDCDC");
                        lnewCell.setText(entry.getKey().getDescription());

                        XWPFRun run4 = lnewCell.getParagraphs().get(0).createRun();
                        run4.setBold(true);

                        lnewRow.getCell(1).setColor("DCDCDC");
                        lnewRow.getCell(2).setColor("DCDCDC");

                        for (Technology technology : entry.getValue()) {

                            XWPFTableRow row = table.createRow();

                            row.getCell(0).setText(technology.getTitle());
                            row.getCell(1).setText(technology.getExperience().toString());
                            row.getCell(2).setText(technology.getLastTimeUsed() == LocalDate.now().getYear() ? "Currently" : technology.getLastTimeUsed().toString());
                        }
                    }
                    break;
                }
            }
        }
    }

    private void replaceLanguages(XWPFDocument doc, List<Language> languages) {

        for (IBodyElement element : doc.getBodyElements()) {

            if (element.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {

                XWPFParagraph paragraph = (XWPFParagraph) element;
                if (CollectionUtils.isNotEmpty(paragraph.getRuns())
                        && paragraph.getRuns().get(0).getText(paragraph.getRuns().get(0).getTextPosition()).equals(ExportPlaceholder.LANGUAGES_PLC_HOLDER)) {

                    paragraph.removeRun(0);

                    XWPFTable table = paragraph.getBody().insertNewTbl(paragraph.getCTP().newCursor());
                    table.getCTTbl().getTblPr().unsetTblBorders();

                    set1Row(table);
                    table.getRow(0).getCell(4).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(3000L));

                    for (Language language : languages) {

                        XWPFTableRow lnewRow = table.createRow();
                        XWPFRun run1 = lnewRow.getCell(4).getParagraphs().get(0).createRun();

                        run1.setBold(true);
                        run1.setText(language.getTitle());

                        XWPFRun run2 = lnewRow.getCell(5).getParagraphs().get(0).createRun();

                        run2.setText(language.getLanguageLevel().getDescription());
                        run2.setColor("777880");
                        run2.setItalic(true);
                    }
                    break;
                }
            }
        }
    }

    private void replaceEducations(XWPFDocument doc, List<Education> educations) {

        for (IBodyElement element : doc.getBodyElements()) {

            if (element.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {

                XWPFParagraph paragraph = (XWPFParagraph) element;
                if (CollectionUtils.isNotEmpty(paragraph.getRuns())
                        && paragraph.getRuns().get(0).getText(paragraph.getRuns().get(0).getTextPosition()).equals(ExportPlaceholder.EDUCATIONS_PLC_HOLDER)) {

                    paragraph.removeRun(0);

                    XWPFTable table = paragraph.getBody().insertNewTbl(paragraph.getCTP().newCursor());
                    table.getCTTbl().getTblPr().unsetTblBorders();

                    set1Row(table);

                    for (int i = 0; i < educations.size(); i++) {

                        Education education = educations.get(i);

                        XWPFRun run1 = table.createRow().getCell(4).getParagraphs().get(0).createRun();

                        run1.setColor("777880");
                        run1.setItalic(true);
                        run1.setText(education.getStartedAt().getYear() + " - " + education.getEndedAt().getYear());

                        XWPFRun run2 = table.createRow().getCell(4).getParagraphs().get(0).createRun();

                        run2.setBold(true);
                        run2.setText(education.getDegree());


                        XWPFRun run3 = table.createRow().getCell(4).getParagraphs().get(0).createRun();
                        run3.setText(education.getSchool());

                        if (i != educations.size() - 1) {
                            table.createRow();
                        }
                    }
                    break;
                }
            }
        }
    }

    private void replaceProjects(XWPFDocument doc, List<Project> projects) {

        for (IBodyElement element : doc.getBodyElements()) {

            if (element.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {

                XWPFParagraph paragraph = (XWPFParagraph) element;
                if (CollectionUtils.isNotEmpty(paragraph.getRuns())
                        && paragraph.getRuns().get(0).getText(paragraph.getRuns().get(0).getTextPosition()).equals(ExportPlaceholder.PROJECTS_PLC_HOLDER)) {

                    paragraph.removeRun(0);

                    XWPFTable table = paragraph.getBody().insertNewTbl(paragraph.getCTP().newCursor());
                    table.getCTTbl().getTblPr().unsetTblBorders();

                    CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
                    width.setType(STTblWidth.DXA);
                    width.setW(BigInteger.valueOf(10700));

                    set1Row(table);

                    for (int i = 0; i < projects.size(); i++) {

                        Project project = projects.get(i);

                        XWPFTableRow row1 = table.createRow();

                        XWPFTableCell cell1 = row1.getCell(4);
                        cell1.setColor("DCDCDC");

                        XWPFRun run1 = cell1.getParagraphs().get(0).createRun();

                        run1.setBold(true);
                        run1.setText("Company:" + project.getCompany());
                        row1.getCell(5).setColor("DCDCDC");

                        XWPFTableRow row2 = table.createRow();

                        XWPFRun run2 = row2.getCell(4).getParagraphs().get(0).createRun();

                        run2.setItalic(true);
                        run2.setColor("777880");
                        run2.setText("Project:");

                        row2.getCell(5).getParagraphs().get(0).createRun().setText(project.getTitle());

                        XWPFTableRow row3 = table.createRow();

                        XWPFRun run3 = row3.getCell(4).getParagraphs().get(0).createRun();

                        run3.setItalic(true);
                        run3.setColor("777880");
                        run3.setText("Project overview:");

                        row3.getCell(5).getParagraphs().get(0).createRun().setText(project.getDescription());

                        XWPFTableRow row4 = table.createRow();

                        XWPFRun run4 = row4.getCell(4).getParagraphs().get(0).createRun();

                        run4.setItalic(true);
                        run4.setColor("777880");
                        run4.setText("Position:");

                        row4.getCell(5).getParagraphs().get(0).createRun().setText(project.getPosition());

                        XWPFTableRow row5 = table.createRow();

                        XWPFRun run5 = row5.getCell(4).getParagraphs().get(0).createRun();

                        run5.setItalic(true);
                        run5.setColor("777880");
                        run5.setText("Team size:");

                        row5.getCell(5).getParagraphs().get(0).createRun().setText(project.getTeamSize().toString());

                        XWPFTableRow row6 = table.createRow();

                        XWPFRun run6 = row6.getCell(4).getParagraphs().get(0).createRun();

                        run6.setItalic(true);
                        run6.setColor("777880");
                        run6.setText("Duration:");

                        row6.getCell(5).getParagraphs().get(0).createRun()
                                .setText(timeUtils.durationString(project.getEndWorkAt(), project.getStartWorkAt(), ChronoUnit.MONTHS));

                        XWPFTableRow row7 = table.createRow();

                        XWPFRun run7 = row7.getCell(4).getParagraphs().get(0).createRun();

                        run7.setItalic(true);
                        run7.setColor("777880");
                        run7.setText("Responsibilities:");

                        responsibilities(row7.getCell(5), Arrays.asList(project.getResponsibilities().split(";")));

                        XWPFTableRow row8 = table.createRow();

                        XWPFRun run8 = row8.getCell(4).getParagraphs().get(0).createRun();

                        run8.setItalic(true);
                        run8.setColor("777880");
                        run8.setText("Technologies:");

                        technologies(row8.getCell(5), new ArrayList<>(project.getProjectTechnologies()));
                        if (i != projects.size() - 1) {

                            table.createRow();
                            table.createRow();
                        }
                    }
                    break;
                }
            }
        }
    }

    private void responsibilities(XWPFTableCell cell, List<String> responsibilities) {

        XWPFParagraph paragraph = cell.getParagraphs().get(0);
        XWPFTable table = cell.insertNewTbl(paragraph.getCTP().newCursor());

        XWPFTableRow row = table.createRow();
        row.addNewTableCell();

        for (String responsibility : responsibilities) {

            XWPFTableRow lnewRow = table.createRow();

            XWPFTableCell lnewRowCell = lnewRow.getCell(0);
            XWPFParagraph lnewPara = lnewRowCell.getParagraphs().get(0);
            lnewPara.setNumID(numID);
            lnewPara.setIndentFromRight(0);

            lnewPara.createRun().setText(responsibility);
        }
        table.removeRow(0);
    }

    private void technologies(XWPFTableCell cell, List<ProjectTechnology> technologies) {

        XWPFParagraph paragraph = cell.getParagraphs().get(0);
        XWPFTable table = cell.insertNewTbl(paragraph.getCTP().newCursor());

        table.getCTTbl().addNewTblPr().addNewTblBorders();

        CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
        width.setType(STTblWidth.DXA);
        width.setW(BigInteger.valueOf(6000));

        setTableAlignment(table, STJc.Enum.forInt(2));

        XWPFTableRow row = table.createRow();
        row.addNewTableCell().getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000L));
        row.addNewTableCell().getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000L));
        row.addNewTableCell().getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000L));

        for (int i = 0; i < technologies.size(); i++) {

            if (i % 3 == 0) {
                row = table.createRow();
            }

            ProjectTechnology technology = technologies.get(i);
            row.getCell(i % 3).setText(technology.getTitle());
        }
        table.removeRow(0);
    }

    private void set1Row(XWPFTable table) {

        XWPFTableRow lnewRow = table.getRow(0);

        lnewRow.addNewTableCell();
        lnewRow.addNewTableCell();
        lnewRow.addNewTableCell();
        lnewRow.addNewTableCell();
        lnewRow.addNewTableCell();
    }

    private void setTableAlignment(XWPFTable table, STJc.Enum justification) {

        CTTblPr tblPr = table.getCTTbl().getTblPr();
        CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
        jc.setVal(justification);
    }

    private Map<TechnologyType, List<Technology>> map(Set<Technology> technologies) {

        if (CollectionUtils.isEmpty(technologies)) {
            return Collections.emptyMap();
        }

        Map<TechnologyType, List<Technology>> technologiesMap = new HashMap<>();
        for (Technology technology : technologies) {

            technologiesMap.putIfAbsent(technology.getType(), new ArrayList<>());
            technologiesMap.get(technology.getType()).add(technology);
        }
        return technologiesMap;
    }

    private void replace(XWPFDocument doc, Map<String, String> data) {
        data.forEach((key, value) -> replace(doc, key, value));
    }

    private XWPFDocument replace(XWPFDocument doc, String placeHolder, String replaceText) {

        for (XWPFHeader header : doc.getHeaderList()) {
            replaceAllBodyElements(header.getBodyElements(), placeHolder, replaceText);
        }
        replaceAllBodyElements(doc.getBodyElements(), placeHolder, replaceText);
        return doc;
    }

    private void replaceAllBodyElements(List<IBodyElement> bodyElements, String placeHolder, String replaceText) {

        for (IBodyElement bodyElement : bodyElements) {
            if (bodyElement.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0)
                replaceParagraph((XWPFParagraph) bodyElement, placeHolder, replaceText);
            if (bodyElement.getElementType().compareTo(BodyElementType.TABLE) == 0)
                replaceTable((XWPFTable) bodyElement, placeHolder, replaceText);
        }
    }

    private void replaceTable(XWPFTable table, String placeHolder, String replaceText) {
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                for (IBodyElement bodyElement : cell.getBodyElements()) {
                    if (bodyElement.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {
                        replaceParagraph((XWPFParagraph) bodyElement, placeHolder, replaceText);
                    }
                    if (bodyElement.getElementType().compareTo(BodyElementType.TABLE) == 0) {
                        replaceTable((XWPFTable) bodyElement, placeHolder, replaceText);
                    }
                }
            }
        }
    }

    private void replaceParagraph(XWPFParagraph paragraph, String placeHolder, String replaceText) {

        for (XWPFRun r : paragraph.getRuns()) {
            String text = r.getText(r.getTextPosition());
            if (text != null && text.contains(placeHolder)) {
                text = text.replace(placeHolder, replaceText);
                r.setText(text, 0);
            }

            String ctr = r.getCTR().toString();
            if (ctr != null && ctr.contains(placeHolder)) {

                ctr = ctr.replace(placeHolder, replaceText);

                XmlObject xmlObject = xmlObject(ctr);
                if (xmlObject != null) {
                    r.getCTR().set(xmlObject);
                }
            }
        }
    }

    private XmlObject xmlObject(String xmlString) {
        try {
            return XmlObject.Factory.parse(xmlString);
        } catch (Exception e) {

            System.out.println("xmlObject exception " + e.getMessage());
            return null;
        }
    }
}
