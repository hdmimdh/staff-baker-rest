package com.oxagile.staffbaker.service;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;

public interface ExportService {

    ResponseEntity<ByteArrayResource> exportWord(Long employeeId);

    ResponseEntity<ByteArrayResource> exportPdf(Long employeeId);
}
