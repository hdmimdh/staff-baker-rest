package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.ProjectTechnologyDto;

import java.util.List;

public interface ProjectTechnologyService {

    void saveAll(Long projectId, List<ProjectTechnologyDto> projectTechnolog);

    void deleteByProjectId(Long projectId);
}
