package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.LanguageReqDto;

import java.util.List;

public interface LanguageService {

    void saveAll(List<LanguageReqDto> reqDtos);
}
