package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.EmployeeReqDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.dto.EmployeeUpdateReqDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeService {

    List<EmployeeRespDto> findAll();

    Page<EmployeeRespDto> findAll(Pageable pageable);

    Page<EmployeeRespDto> findAll(String query, Pageable pageable);

    EmployeeRespDto find(Long id);

    Long save(EmployeeReqDto employeeReqDto);

    void update(Long id, EmployeeUpdateReqDto employeeRequestDto);

    void updateSummary(Long id, String summary);

    void deactivate(Long id);
}
