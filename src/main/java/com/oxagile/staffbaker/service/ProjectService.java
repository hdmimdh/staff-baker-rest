package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.ProjectRequestDto;
import com.oxagile.staffbaker.dto.ProjectResponseDto;

import java.util.List;

public interface ProjectService {

    void save(ProjectRequestDto requestDto);

    void update(Long id, ProjectRequestDto requestDto);

    List<ProjectResponseDto> findByEmployee(Long employeeId);

    void delete(Long id);
}
