package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.domain.SchoolTemplate;
import com.oxagile.staffbaker.dto.SchoolTemplateReqDto;

import java.util.List;

public interface SchoolTemplateService {

    void saveAll(List<SchoolTemplateReqDto> reqDtos);

    List<SchoolTemplate> findAll();
}
