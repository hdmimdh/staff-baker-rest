package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.ProjectTemplateDto;
import com.oxagile.staffbaker.dto.ProjectTemplateReqDto;

import java.util.List;

public interface ProjectTemplateService {

    List<ProjectTemplateDto> findAll();

    ProjectTemplateDto find(Long id);

    Long save(ProjectTemplateReqDto requestDto);

    void update(Long id, ProjectTemplateReqDto requestDto);

    void deactivate(Long id);
}
