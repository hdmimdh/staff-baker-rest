package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.TechnologyTemplate;
import com.oxagile.staffbaker.domain.TechnologyType;
import com.oxagile.staffbaker.dto.TechnologyTemplateDto;
import com.oxagile.staffbaker.dto.TechnologyTemplateReqDto;
import com.oxagile.staffbaker.repository.TechnologyTemplateRepository;
import com.oxagile.staffbaker.service.TechnologyTemplateService;
import org.apache.commons.lang3.EnumUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TechnologyTemplateServiceImpl implements TechnologyTemplateService {
    private static final Logger LOGGER = LogManager.getLogger(TechnologyTemplateServiceImpl.class);

    private final ConversionService conversionService;
    private final TechnologyTemplateRepository technologyTemplateRepository;

    @Autowired
    public TechnologyTemplateServiceImpl(ConversionService conversionService, TechnologyTemplateRepository technologyTemplateRepository) {
        this.conversionService = conversionService;
        this.technologyTemplateRepository = technologyTemplateRepository;
    }

    @Override
    public List<TechnologyTemplateDto> findAll() {

        List<TechnologyTemplate> templates = technologyTemplateRepository.findAll();
        if (CollectionUtils.isEmpty(templates)) {

            LOGGER.info("Unable to find at least one project template.");
            return Collections.emptyList();
        }

        return templates.stream()
                .map(template -> conversionService.convert(template, TechnologyTemplateDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Long save(TechnologyTemplateReqDto requestDto) {

        TechnologyTemplate template = technologyTemplateRepository.findByTitle(requestDto.getTitle());
        if (template != null) {

            LOGGER.error("Unable to save technology template with non unique title used.");
            throw new IllegalArgumentException("Unable to save technology template with non unique title used.");
        }

        template = TechnologyTemplate.newBuilder()
                .withType(EnumUtils.getEnum(TechnologyType.class, requestDto.getType()))
                .withTitle(requestDto.getTitle()).build();

        return technologyTemplateRepository.save(template).getId();
    }

    @Override
    public void update(Long id, TechnologyTemplateReqDto requestDto) {

        TechnologyTemplate template = technologyTemplateRepository.findOne(id);
        if (template == null) {

            LOGGER.error("Unable to find technology template with id of {} to be modified.", id);
            throw new IllegalArgumentException("Unable to find technology template with id of " + id + " to be modified.");
        }

        template.setTitle(requestDto.getTitle());
        template.setType(EnumUtils.getEnum(TechnologyType.class, requestDto.getType()));
        template.setId(id);

        technologyTemplateRepository.save(template);
    }

    @Override
    public void delete(Long id) {

        if (id == null) {

            LOGGER.error("Unable to delete technology template with id of null.");
            throw new IllegalArgumentException("Unable to delete technology template with id of null.");
        }
        technologyTemplateRepository.delete(id);
    }
}
