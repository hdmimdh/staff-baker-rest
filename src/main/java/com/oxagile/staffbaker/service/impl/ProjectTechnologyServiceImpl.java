package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.ProjectTechnology;
import com.oxagile.staffbaker.dto.ProjectTechnologyDto;
import com.oxagile.staffbaker.repository.ProjectTechnologyRepository;
import com.oxagile.staffbaker.service.ProjectTechnologyService;
import com.oxagile.staffbaker.service.converter.ProjectTechnologyDtoConverter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectTechnologyServiceImpl implements ProjectTechnologyService {

    private static final Logger LOGGER = LogManager.getLogger(ProjectTechnologyServiceImpl.class);

    private final ProjectTechnologyRepository projectTechnologyRepository;

    private final ProjectTechnologyDtoConverter technologyDtoConverter;

    @Autowired
    public ProjectTechnologyServiceImpl(ProjectTechnologyRepository projectTechnologyRepository, ProjectTechnologyDtoConverter technologyDtoConverter) {
        this.projectTechnologyRepository = projectTechnologyRepository;
        this.technologyDtoConverter = technologyDtoConverter;
    }

    @Override
    public void saveAll(Long projectId, List<ProjectTechnologyDto> technologyDtos) {

        if (CollectionUtils.isEmpty(technologyDtos)) {
            return;
        }

        List<ProjectTechnology> projectTechnologies = technologyDtoConverter.convert(projectId, technologyDtos);
        projectTechnologyRepository.save(projectTechnologies);
    }

    @Override
    public void deleteByProjectId(Long projectId) {

        if (projectId == null) {
            return;
        }
        projectTechnologyRepository.deleteProjectTechnologyByProjectId(projectId);
    }
}
