package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.domain.Language;
import com.oxagile.staffbaker.domain.LanguageLevel;
import com.oxagile.staffbaker.dto.LanguageReqDto;
import com.oxagile.staffbaker.repository.LanguageRepository;
import com.oxagile.staffbaker.service.LanguageService;
import org.apache.commons.lang3.EnumUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageServiceImpl implements LanguageService {

    private static final Logger LOGGER = LogManager.getLogger(LanguageServiceImpl.class);

    private final LanguageRepository languageRepository;

    @Autowired
    public LanguageServiceImpl(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Override
    public void saveAll(List<LanguageReqDto> reqDtos) {

        if (CollectionUtils.isEmpty(reqDtos)) {
            return;
        }

        languageRepository.save(
                reqDtos.stream()
                        .map(
                                dto -> Language.newBuilder()
                                        .withTitle(dto.getTitle())
                                        .withLanguageLevel(EnumUtils.getEnum(LanguageLevel.class, dto.getLanguageLevel()))
                                        .withEmployee(Employee.newBuilder().withId(dto.getEmployeeId()).build())
                                        .build())
                        .collect(Collectors.toList())
        );
    }
}
