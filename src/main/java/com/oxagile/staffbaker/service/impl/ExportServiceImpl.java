package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.repository.EmployeeRepository;
import com.oxagile.staffbaker.service.ExportService;
import com.oxagile.staffbaker.service.export.WordExportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class ExportServiceImpl implements ExportService {

    private static final Logger LOGGER = LogManager.getLogger(ExportServiceImpl.class);

    private final EmployeeRepository employeeRepository;
    private final WordExportService wordExportService;

    @Autowired
    public ExportServiceImpl(EmployeeRepository employeeRepository, WordExportService wordExportService) {
        this.employeeRepository = employeeRepository;
        this.wordExportService = wordExportService;
    }

    @Override
    public ResponseEntity<ByteArrayResource> exportWord(Long employeeId) {

        if (employeeId == null) {

            LOGGER.error("Employee id can't be null.");
            return ResponseEntity.badRequest().build();
        }

        Employee employee = employeeRepository.findEmployeeFullById(employeeId);
        if (employee == null) {

            LOGGER.info("Employee not found by id of {}", employeeId);
            return ResponseEntity.noContent().build();
        }

        try {

            XWPFDocument document = wordExportService.export(employee);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            document.write(byteArrayOutputStream);

            ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "attachment; filename=" + filename(employee, ".docx"));
            headers.set("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            headers.set("Expires", "0");
            headers.set("Pragma", "no-cache");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document"))
                    .body(resource);

        } catch (InvalidFormatException | IOException e) {

            LOGGER.error("Unable to export employee of {} due to {}.", employeeId, e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String filename(Employee employee, String extension) {
        return "Oxagile CV - " + employee.getFirstName() + " " + employee.getLastName().substring(0, 1) + " - " + employee.getTitle() + extension;
    }

    @Override
    public ResponseEntity<ByteArrayResource> exportPdf(Long employeeId) {
        return null;
    }
}
