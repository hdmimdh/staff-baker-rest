package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.ProjectTemplate;
import com.oxagile.staffbaker.dto.ProjectTemplateDto;
import com.oxagile.staffbaker.dto.ProjectTemplateReqDto;
import com.oxagile.staffbaker.repository.ProjectTemplateRepository;
import com.oxagile.staffbaker.service.ProjectTemplateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectTemplateServiceImpl implements ProjectTemplateService {

    private static final Logger LOGGER = LogManager.getLogger(ProjectTemplateServiceImpl.class);

    private final ConversionService conversionService;
    private final ProjectTemplateRepository projectTemplateRepository;

    @Autowired
    public ProjectTemplateServiceImpl(ProjectTemplateRepository projectTemplateRepository, ConversionService conversionService) {
        this.projectTemplateRepository = projectTemplateRepository;
        this.conversionService = conversionService;
    }

    @Override
    public List<ProjectTemplateDto> findAll() {

        List<ProjectTemplate> projectTemplates = projectTemplateRepository.findAll();
        if (CollectionUtils.isEmpty(projectTemplates)) {

            LOGGER.info("Can not find any project templates.");
            return Collections.emptyList();
        }

        return projectTemplates.stream()
                .map(projectTemplate -> conversionService.convert(projectTemplate, ProjectTemplateDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ProjectTemplateDto find(Long id) {

        ProjectTemplate projectTemplate = projectTemplateRepository.findOne(id);
        if (projectTemplate == null) {

            LOGGER.info("There is no project template with the given id {}.", id);
            return null;
        }

        return conversionService.convert(projectTemplate, ProjectTemplateDto.class);
    }

    @Override
    public Long save(ProjectTemplateReqDto requestDto) {

        ProjectTemplate projectTemplate = projectTemplateRepository.findByTitle(requestDto.getTitle());
        if (projectTemplate != null) {

            LOGGER.info("Attempt to save project template with existed name - {}", projectTemplate.getDescription());
            throw new IllegalArgumentException("Attempt to save project template with existing name - " + projectTemplate.getDescription());
        }

        projectTemplate = conversionService.convert(requestDto, ProjectTemplate.class);
        return projectTemplateRepository.save(projectTemplate).getId();
    }

    @Override
    public void update(Long id, ProjectTemplateReqDto requestDto) {

        if (!projectTemplateRepository.exists(id)) {

            LOGGER.info("Unable to update project template with the given id {}.", id);
            throw new IllegalArgumentException("Unable to find iproject template with id of " + id + " to be modified.");
        }

        ProjectTemplate projectTemplate = conversionService.convert(requestDto, ProjectTemplate.class);
        projectTemplate.setId(id);

        projectTemplateRepository.save(projectTemplate);
    }

    @Override
    public void deactivate(Long id) {

        ProjectTemplate projectTemplate = projectTemplateRepository.findOne(id);
        if (projectTemplate == null) {

            LOGGER.info("Unable to deactivate project template with id {} because it does not exist.");
            throw new IllegalArgumentException("Unable to deactivate project template with id " + id + " because it does not exist.");
        }

        projectTemplate.setActive(false);
        projectTemplate.setDeactivatedAt(ZonedDateTime.now(ZoneOffset.UTC));

        projectTemplateRepository.save(projectTemplate);
    }
}
