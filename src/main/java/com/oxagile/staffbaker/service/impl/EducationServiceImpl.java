package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Education;
import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.EducationReqDto;
import com.oxagile.staffbaker.repository.EducationRepository;
import com.oxagile.staffbaker.service.EducationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EducationServiceImpl implements EducationService {

    private static final Logger LOGGER = LogManager.getLogger(EducationServiceImpl.class);

    private final EducationRepository educationRepository;

    @Autowired
    public EducationServiceImpl(EducationRepository educationRepository) {
        this.educationRepository = educationRepository;
    }

    @Override
    public void saveAll(List<EducationReqDto> reqDtos) {

        if (CollectionUtils.isEmpty(reqDtos)) {
            return;
        }

        educationRepository.save(
                reqDtos.stream()
                        .map(
                                dto -> Education.newBuilder()
                                        .withSchool(dto.getSchool())
                                        .withDegree(dto.getDegree())
                                        .withEmployee(Employee.newBuilder().withId(dto.getEmployeeId()).build())
                                        .withStartedAt(dto.getStartedAt())
                                        .withEndedAt(dto.getEndedAt())
                                        .build())
                        .collect(Collectors.toList())
        );
    }
}
