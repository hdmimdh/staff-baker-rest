package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.EmployeeReqDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.dto.EmployeeUpdateReqDto;
import com.oxagile.staffbaker.repository.EmployeeRepository;
import com.oxagile.staffbaker.service.EmployeeService;
import com.oxagile.staffbaker.service.converter.EmployeeDtoToEmployeeUpdater;
import com.oxagile.staffbaker.service.search.CustomRsqlVisitor;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private static final Logger LOGGER = LogManager.getLogger(EmployeeServiceImpl.class);

    private final EmployeeRepository employeeRepository;
    private final ConversionService conversionService;
    private final EmployeeDtoToEmployeeUpdater updater;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, ConversionService conversionService, EmployeeDtoToEmployeeUpdater updater) {
        this.employeeRepository = employeeRepository;
        this.conversionService = conversionService;
        this.updater = updater;
    }

    @Override
    public List<EmployeeRespDto> findAll() {

        List<Employee> employees = employeeRepository.findAll();
        if (CollectionUtils.isEmpty(employees)) {

            LOGGER.info("Unable to find at least one employee.");
            return Collections.emptyList();
        }

        return employees.stream()
                .map(employee -> conversionService.convert(employee, EmployeeRespDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Page<EmployeeRespDto> findAll(Pageable pageable) {

        Page<Employee> page = employeeRepository.findAll(pageable);
        return convert(page, pageable);
    }

    @Override
    public Page<EmployeeRespDto> findAll(String query, Pageable pageable) {

        try {

            Node rootNode = new RSQLParser().parse(query);
            Specification<Employee> specification = rootNode.accept(new CustomRsqlVisitor<Employee>());

            Page<Employee> page = employeeRepository.findAll(specification, pageable);
            return convert(page, pageable);

        } catch (Exception e) {

            LOGGER.error("Unable to resolve query string due to {}", e.getMessage());
            throw new IllegalArgumentException("Unable to resolve query string due to " + e.getMessage());
        }
    }

    private Page<EmployeeRespDto> convert(Page<Employee> page, Pageable pageable) {

        if (CollectionUtils.isEmpty(page.getContent())) {

            LOGGER.info("Unable to find at least one employee.");
            return new PageImpl<>(Collections.emptyList(), pageable, 0);
        }

        List<EmployeeRespDto> employees = page.getContent().stream()
                .map(employee -> conversionService.convert(employee, EmployeeRespDto.class))
                .collect(Collectors.toList());

        return new PageImpl<>(employees, pageable, page.getTotalElements());
    }

    @Override
    public EmployeeRespDto find(Long id) {

        Employee employee = employeeRepository.findOne(id);
        if (employee == null) {

            LOGGER.info("Unable to find employee by id of {}.", id);
            return null;
        }

        return conversionService.convert(employee, EmployeeRespDto.class);
    }

    @Override
    public Long save(EmployeeReqDto employeeReqDto) {

        Employee employee = employeeRepository.findEmployeeByEmail(employeeReqDto.getEmail());
        if (employee != null) {

            LOGGER.error("Unable to save employee due to non unique email used.");
            throw new IllegalArgumentException("Unable to save employee due to non unique email used.");
        }

        employee = conversionService.convert(employeeReqDto, Employee.class);

        return employeeRepository.save(employee).getId();
    }

    @Override
    public void update(Long id, EmployeeUpdateReqDto employeeRequestDto) {

        Employee employee = employeeRepository.findOne(id);
        if (employee == null) {

            LOGGER.error("Unable to find employee with id of {} to be modified.", id);
            throw new IllegalArgumentException("Unable to find employee with id of " + id + " to be modified.");
        }

        employee = updater.convert(employeeRequestDto, employee);

        employee.setId(id);
        employeeRepository.save(employee);
    }

    @Override
    public void updateSummary(Long id, String summary) {

        Employee employee = employeeRepository.findOne(id);
        if (employee == null) {

            LOGGER.error("Unable to find employee with id of {} to be modified.", id);
            throw new IllegalArgumentException("Unable to find employee with id of " + id + " to be modified.");
        }

        employee.setSummary(summary);
        employeeRepository.save(employee);
    }

    @Override
    public void deactivate(Long id) {

        Employee employee = employeeRepository.findOne(id);
        if (employee == null) {

            LOGGER.error("Unable to find employee with id of {} to be deactivated.", id);
            throw new IllegalArgumentException("Unable to find employee with id of " + id + " to be deactivated.");
        }

        employee.setActive(false);
        employee.setDeactivatedAt(ZonedDateTime.now(ZoneOffset.UTC));
        employeeRepository.save(employee);
    }
}
