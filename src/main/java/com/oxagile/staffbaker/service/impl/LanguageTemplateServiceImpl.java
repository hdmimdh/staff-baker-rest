package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.LanguageTemplate;
import com.oxagile.staffbaker.dto.LanguageTemplateReqDto;
import com.oxagile.staffbaker.repository.LanguageTemplateRepository;
import com.oxagile.staffbaker.service.LanguageTemplateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageTemplateServiceImpl implements LanguageTemplateService {

    private static final Logger LOGGER = LogManager.getLogger(LanguageTemplateServiceImpl.class);

    private final LanguageTemplateRepository languageTemplateRepository;

    @Autowired
    public LanguageTemplateServiceImpl(LanguageTemplateRepository languageTemplateRepository) {
        this.languageTemplateRepository = languageTemplateRepository;
    }

    @Override
    public void saveAll(List<LanguageTemplateReqDto> reqDtos) {

        if (CollectionUtils.isEmpty(reqDtos)) {
            return;
        }

        languageTemplateRepository.save(
                reqDtos.stream()
                        .map(dto -> LanguageTemplate.newBuilder().withTitle(dto.getTitle()).build())
                        .collect(Collectors.toList())
        );
    }

    @Override
    public List<LanguageTemplate> findAll() {

        List<LanguageTemplate> languageTemplates = languageTemplateRepository.findAll();
        if (CollectionUtils.isEmpty(languageTemplates)) {

            LOGGER.info("Can not find any language template.");
            return Collections.emptyList();
        }

        return languageTemplates;
    }
}
