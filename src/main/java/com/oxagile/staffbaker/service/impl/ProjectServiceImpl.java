package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Project;
import com.oxagile.staffbaker.dto.ProjectRequestDto;
import com.oxagile.staffbaker.dto.ProjectResponseDto;
import com.oxagile.staffbaker.repository.ProjectRepository;
import com.oxagile.staffbaker.service.ProjectService;
import com.oxagile.staffbaker.service.ProjectTechnologyService;
import com.oxagile.staffbaker.service.converter.ProjectFromDtoConverter;
import com.oxagile.staffbaker.service.converter.ProjectToResponseDtoConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOGGER = LogManager.getLogger(ProjectServiceImpl.class);

    private final ProjectRepository projectRepository;

    private final ProjectToResponseDtoConverter projectToResponseDtoConverter;

    private final ProjectFromDtoConverter projectFromDtoConverter;

    private final ProjectTechnologyService projectTechnologyService;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectToResponseDtoConverter projectToResponseDtoConverter, ProjectFromDtoConverter projectFromDtoConverter, ProjectTechnologyService projectTechnologyService) {
        this.projectRepository = projectRepository;
        this.projectToResponseDtoConverter = projectToResponseDtoConverter;
        this.projectFromDtoConverter = projectFromDtoConverter;
        this.projectTechnologyService = projectTechnologyService;
    }

    @Transactional
    @Override
    public void save(ProjectRequestDto requestDto) {

        if (requestDto == null) {
            return;
        }

        Project project = projectFromDtoConverter.convert(requestDto);

        projectRepository.save(project);
        projectTechnologyService.saveAll(project.getId(), requestDto.getProjectTechnologies());
    }

    @Transactional
    @Override
    public void update(Long id, ProjectRequestDto requestDto) {

        boolean exists = projectRepository.exists(id);
        if (!exists) {

            LOGGER.error("Unable to find project with id of {} to be modified.", id);
            throw new IllegalArgumentException("Unable to find project with id of " + id + " to be modified.");
        }

        Project project = projectFromDtoConverter.convertWithTechnologies(id, requestDto);
        projectRepository.save(project);
    }

    @Override
    public List<ProjectResponseDto> findByEmployee(Long employeeId) {

        Set<Project> projects = projectRepository.findAllByEmployeeId(employeeId);
        if (CollectionUtils.isEmpty(projects)) {
            return Collections.emptyList();
        }

        return projectToResponseDtoConverter.convert(projects);
    }

    @Override
    public void delete(Long id) {

        if (id == null) {
            return;
        }
        projectRepository.delete(id);
    }
}
