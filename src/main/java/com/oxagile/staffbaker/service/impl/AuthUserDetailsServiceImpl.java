package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.repository.EmployeeRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthUserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOGGER = LogManager.getLogger(AuthUserDetailsServiceImpl.class);

    private final EmployeeRepository employeeRepository;

    @Autowired
    public AuthUserDetailsServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        if (StringUtils.isBlank(email)) {
            return null;
        }

        Employee employee = employeeRepository.findEmployeeFullByEmail(email);
        if (employee == null) {

            LOGGER.error("Unable to find employee by email {}", email);
            throw new UsernameNotFoundException("Unable to find employee by email " + email);
        }
        return employee;
    }
}
