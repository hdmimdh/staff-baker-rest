package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.DegreeTemplate;
import com.oxagile.staffbaker.dto.DegreeTemplateReqDto;
import com.oxagile.staffbaker.repository.DegreeTemplateRepository;
import com.oxagile.staffbaker.service.DegreeTemplateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DegreeTemplateServiceImpl implements DegreeTemplateService {

    private static final Logger LOGGER = LogManager.getLogger(DegreeTemplateServiceImpl.class);

    private final DegreeTemplateRepository degreeTemplateRepository;

    @Autowired
    public DegreeTemplateServiceImpl(DegreeTemplateRepository degreeTemplateRepository) {
        this.degreeTemplateRepository = degreeTemplateRepository;
    }

    @Override
    public void saveAll(List<DegreeTemplateReqDto> reqDtos) {

        if (CollectionUtils.isEmpty(reqDtos)) {
            return;
        }

        degreeTemplateRepository.save(
                reqDtos.stream()
                        .map(dto -> DegreeTemplate.newBuilder().withTitle(dto.getTitle()).build())
                        .collect(Collectors.toList())
        );
    }

    @Override
    public List<DegreeTemplate> findAll() {

        List<DegreeTemplate> degreeTemplates = degreeTemplateRepository.findAll();
        if (CollectionUtils.isEmpty(degreeTemplates)) {

            LOGGER.info("Can not find any degree template.");
            return Collections.emptyList();
        }

        return degreeTemplates;
    }
}
