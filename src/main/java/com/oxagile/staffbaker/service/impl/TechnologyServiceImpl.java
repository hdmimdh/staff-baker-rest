package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Technology;
import com.oxagile.staffbaker.dto.TechnologyReqDto;
import com.oxagile.staffbaker.repository.TechnologyRepository;
import com.oxagile.staffbaker.service.TechnologyService;
import com.oxagile.staffbaker.service.converter.TechnologyFromDtoConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TechnologyServiceImpl implements TechnologyService {

    private static final Logger LOGGER = LogManager.getLogger(TechnologyServiceImpl.class);

    private final TechnologyFromDtoConverter converter;
    private final TechnologyRepository technologyRepository;

    @Autowired
    public TechnologyServiceImpl(TechnologyFromDtoConverter converter, TechnologyRepository technologyRepository) {
        this.converter = converter;
        this.technologyRepository = technologyRepository;
    }

    @Override
    public void saveAll(List<TechnologyReqDto> technologiesDtos) {

        if (CollectionUtils.isEmpty(technologiesDtos)) {
            return;
        }

        LOGGER.info("Saving batch of technologies of size of {}", technologiesDtos.size());
        List<Technology> technologies = converter.convert(technologiesDtos);
        if (CollectionUtils.isEmpty(technologies)) {
            return;
        }
        technologyRepository.save(technologies);
    }

    @Override
    public void updateAll(Long employeeId, List<TechnologyReqDto> technologiesDtos) {

        if (CollectionUtils.isEmpty(technologiesDtos)) {
            return;
        }

        LOGGER.info("Updating batch of technologies of size of {}", technologiesDtos.size());
        List<Technology> technologies = converter.convert(employeeId, technologiesDtos);
        if (CollectionUtils.isEmpty(technologies)) {
            return;
        }
        technologyRepository.save(technologies);
    }

    @Override
    public void deleteAll(List<Long> ids) {

        if (CollectionUtils.isEmpty(ids)) {
            return;
        }

        LOGGER.info("Deleting batch of technologies of size of {}", ids.size());

        List<Technology> technologies = ids.stream()
                .map(id -> Technology.newBuilder().withId(id).build())
                .collect(Collectors.toList());

        technologyRepository.delete(technologies);
    }
}
