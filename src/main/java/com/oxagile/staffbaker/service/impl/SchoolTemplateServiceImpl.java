package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.SchoolTemplate;
import com.oxagile.staffbaker.dto.SchoolTemplateReqDto;
import com.oxagile.staffbaker.repository.SchoolTemplateRepository;
import com.oxagile.staffbaker.service.SchoolTemplateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SchoolTemplateServiceImpl implements SchoolTemplateService {

    private static final Logger LOGGER = LogManager.getLogger(SchoolTemplateServiceImpl.class);

    private final SchoolTemplateRepository schoolTemplateRepository;

    @Autowired
    public SchoolTemplateServiceImpl(SchoolTemplateRepository schoolTemplateRepository) {
        this.schoolTemplateRepository = schoolTemplateRepository;
    }

    @Override
    public void saveAll(List<SchoolTemplateReqDto> reqDtos) {

        if (CollectionUtils.isEmpty(reqDtos)) {
            return;
        }

        schoolTemplateRepository.save(
                reqDtos.stream()
                        .map(dto -> SchoolTemplate.newBuilder().withTitle(dto.getTitle()).build())
                        .collect(Collectors.toList())
        );
    }

    @Override
    public List<SchoolTemplate> findAll() {

        List<SchoolTemplate> schoolTemplates = schoolTemplateRepository.findAll();
        if (CollectionUtils.isEmpty(schoolTemplates)) {

            LOGGER.info("Can not find any school template.");
            return Collections.emptyList();
        }

        return schoolTemplates;
    }
}
