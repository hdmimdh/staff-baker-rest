package com.oxagile.staffbaker.service.impl;

import com.oxagile.staffbaker.domain.Employee;
import com.oxagile.staffbaker.dto.CredentialsDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;
import com.oxagile.staffbaker.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final ConversionService conversionService;

    @Autowired
    public AuthServiceImpl(AuthenticationManager authenticationManager, ConversionService conversionService) {
        this.authenticationManager = authenticationManager;
        this.conversionService = conversionService;
    }

    @Override
    public EmployeeRespDto login(CredentialsDto credentials) {

        UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword());
        Authentication authentication = this.authenticationManager.authenticate(loginToken);
        if (authentication == null) {
            return null;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        Employee employee = (Employee) authentication.getPrincipal();

        return conversionService.convert(employee, EmployeeRespDto.class);
    }
}
