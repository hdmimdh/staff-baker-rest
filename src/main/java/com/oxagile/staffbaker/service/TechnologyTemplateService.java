package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.TechnologyTemplateDto;
import com.oxagile.staffbaker.dto.TechnologyTemplateReqDto;

import java.util.List;

public interface TechnologyTemplateService {

    List<TechnologyTemplateDto> findAll();

    Long save(TechnologyTemplateReqDto requestDto);

    void update(Long id, TechnologyTemplateReqDto requestDto);

    void delete(Long id);
}
