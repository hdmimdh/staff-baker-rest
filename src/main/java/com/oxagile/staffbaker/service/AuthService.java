package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.dto.CredentialsDto;
import com.oxagile.staffbaker.dto.EmployeeRespDto;

public interface AuthService {

    EmployeeRespDto login(CredentialsDto credentials);
}
