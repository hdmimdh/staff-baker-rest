package com.oxagile.staffbaker.service;

import com.oxagile.staffbaker.domain.LanguageTemplate;
import com.oxagile.staffbaker.dto.LanguageTemplateReqDto;

import java.util.List;

public interface LanguageTemplateService {

    void saveAll(List<LanguageTemplateReqDto> reqDtos);

    List<LanguageTemplate> findAll();
}
