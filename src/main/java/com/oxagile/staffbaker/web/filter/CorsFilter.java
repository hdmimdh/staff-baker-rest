package com.oxagile.staffbaker.web.filter;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CorsFilter extends GenericFilterBean {

    private static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME = "Access-Control-Allow-Origin";
    private static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_VALUE = "*";
    private static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER_NAME = "Access-Control-Allow-Methods";
    private static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER_VALUE = "POST, PUT, GET, OPTIONS, DELETE";
    private static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER_NAME = "Access-Control-Allow-Headers";
    private static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER_VALUE = "Content-Type, Authorization, Origin";
    private static final String ACCESS_CONTROL_EXPOSE_HEADERS_HEADER_NAME = "Access-Control-Expose-Headers";
    private static final String OPTIONS_METHOD_NAME = "OPTIONS";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME, ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_VALUE);
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_METHODS_HEADER_NAME, ACCESS_CONTROL_ALLOW_METHODS_HEADER_VALUE);
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_HEADERS_HEADER_NAME, ACCESS_CONTROL_ALLOW_HEADERS_HEADER_VALUE);
        httpResponse.setHeader(ACCESS_CONTROL_EXPOSE_HEADERS_HEADER_NAME, ACCESS_CONTROL_ALLOW_HEADERS_HEADER_VALUE);

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        if (httpRequest.getMethod().equalsIgnoreCase(OPTIONS_METHOD_NAME)) {
            httpResponse.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
